import pandas as pd
import json
import time
import numpy as np
from kedro.runner import SequentialRunner
from kedro.io import DataCatalog, MemoryDataSet
from kedro.extras.datasets.tensorflow import TensorFlowModelDataset
from kedro.extras.datasets.yaml import YAMLDataSet
from kedro.extras.datasets.pandas import CSVDataSet
from kedro.extras.datasets.pickle import PickleDataSet
import bios
import os
import sys

# Set up paths
module_path = os.path.abspath(os.path.join("../analytics"))
if module_path not in sys.path:
    sys.path.append(module_path)
from src.wyre.composers.pipelines_split import *

# transactions = pd.read_csv("data/01_raw/synthetic/input_transactions_for_splitting.csv")
# accounts = pd.read_csv("data/01_raw/synthetic/input_accounts_for_splitting.csv")

with open("data/01_raw/synthetic/test_optimization/account.json") as f:
    data = json.load(f)
accounts = pd.DataFrame(data)

with open("data/01_raw/synthetic/test_optimization/transaction.json") as f:
    data = json.load(f)
transactions = pd.DataFrame(data)
transactions = transactions[0:1200]


def fix_date(date):
    fixed_date = date["$date"]
    return fixed_date[:-1]


def fix_id(id):
    fixed_id = id["$oid"]
    return fixed_id


transactions
transactions["date"] = transactions["date"].apply(lambda x: fix_date(x))
transactions["_id"] = transactions["_id"].apply(lambda x: fix_id(x))
transactions["date_updated"] = transactions["date_updated"].apply(lambda x: fix_date(x))
transactions["date_created"] = transactions["date_created"].apply(lambda x: fix_date(x))

transactions = transactions.drop(
    ["classification", "confidence", "is_recurring", "group", "duplicated_with"], axis=1
)

accounts["pan"] = str(accounts["pan"])


def data_catalog(
    input_transactions: pd.DataFrame,
    input_accounts: pd.DataFrame,
) -> DataCatalog:
    parameters = bios.read("conf/base/parameters.yml")
    return DataCatalog(
        {
            # MemoryDataSet
            "input_accounts": MemoryDataSet(input_accounts),
            "input_transactions": MemoryDataSet(input_transactions),
            "params:incoming_description_classifier_model_params": MemoryDataSet(
                parameters["incoming_description_classifier_model_params"]
            ),
            "params:is_income": MemoryDataSet(parameters["is_income"]),
            "params:is_expense": MemoryDataSet(parameters["is_expense"]),
            "params:is_training": MemoryDataSet(True),
            "params:card_expenses_description_classifier_model_params": MemoryDataSet(
                parameters["card_expenses_description_classifier_model_params"]
            ),
            "params:remittance_expenses_description_classifier_model_params": MemoryDataSet(
                parameters["remittance_expenses_description_classifier_model_params"]
            ),
            "params:banks_to_change": MemoryDataSet(parameters["banks_to_change"]),
            "params:alpha_bank_name": MemoryDataSet(parameters["alpha_bank_name"]),
            "params:nbg_bank_name": MemoryDataSet(parameters["nbg_bank_name"]),
            "params:other_category_threshold": MemoryDataSet(
                parameters["other_category_threshold"]
            ),
            # TensorFlowModelDataset
            "incoming_description_classifier_model": TensorFlowModelDataset(
                "data/06_models/incoming_description_classifier_model"
            ),
            "transaction_classifier_model": TensorFlowModelDataset(
                "data/06_models/transaction_classifier_model"
            ),
            "card_expenses_description_classifier_model": TensorFlowModelDataset(
                "data/06_models/card_expenses_description_classifier_model"
            ),
            "remittance_expenses_description_classifier_model": TensorFlowModelDataset(
                "data/06_models/remittance_expenses_description_classifier_model"
            ),
            # PickleDataSet
            "incoming_description_tokenizer": PickleDataSet(
                "data/06_models/incoming_description_tokenizer/incoming_description_tokenizer.pickle"
            ),
            # YAMLDataSet
            "incoming_description_classifier_y_labels": YAMLDataSet(
                "data/06_models/labels/incoming_description_classifier_y_labels.yaml"
            ),
            "transaction_classifier_y_labels": YAMLDataSet(
                "data/05_model_input/transaction_classifier_model/transaction_classifier_y_labels.yaml"
            ),
            "card_expenses_description_classifier_y_labels": YAMLDataSet(
                "data/06_models/labels/card_expenses_description_classifier_y_labels.yaml"
            ),
            "remittance_expenses_description_classifier_y_labels": YAMLDataSet(
                "data/06_models/labels/remittance_expenses_description_classifier_y_labels.yaml"
            ),
            # CSVDataSet
            "expenses_categories_bucket": CSVDataSet(
                "data/04_feature/bag_of_words/expenses_categories_bucket.csv"
            ),
            "essential_lifestyle_wysely_categories": CSVDataSet(
                "data/04_feature/bag_of_words/essential_lifestyle_wysely_categories.csv"
            ),
            "bank_digits": CSVDataSet(
                "data/04_feature/bag_of_words/bank_digits.csv", load_args=dict(dtype=object)
            ),
            "mcc_codes_to_wysely_categories": CSVDataSet(
                "data/04_feature/bag_of_words/mcc_codes_to_wysely_categories.csv"
            ),
            "card_expenses_description_tokenizer": PickleDataSet(
                "data/06_models/card_expenses_description_tokenizer/card_expenses_description_tokenizer.pickle"
            ),
            "card_expenses_words_to_remove": CSVDataSet(
                "data/04_feature/bag_of_words/card_expenses_words_to_remove.csv"
            ),
            "remittance_expenses_description_tokenizer": PickleDataSet(
                "data/06_models/remittance_expenses_description_tokenizer/remittance_expenses_description_tokenizer.pickle"
            ),
        }
    )


runner = SequentialRunner()


def main():
    start = time.time()
    catalog = data_catalog(transactions, accounts)
    # Create pipeline
    pipeline = create_classify_transactions_full_pipeline()
    # Run pipeline
    result = runner.run(pipeline, catalog)
    end = time.time()
    print("Run for ", len(transactions), " rows for: ", end - start)
    print(result)
    # print(
    #    "basic_classified_incoming_transactions ", result["basic_classified_incoming_transactions"]
    # )
    # print("mcc_card_expenses_output_transactions ", result["mcc_card_expenses_output_transactions"])
    # print(
    #    "non_mcc_card_expenses_output_transactions ",
    #    result["non_mcc_card_expenses_output_transactions"],
    # )
    # print(
    #    "remittance_expenses_output_transactions ",
    #    result["remittance_expenses_output_transactions"],
    # )
    # print(
    #    "basic_classified_expenses_transactions",
    #    result["basic_classified_expenses_transactions"],
    ##)
    # print(
    #    "full_classification_pipeline_output",
    #    result["full_classification_pipeline_output"],
    # )


if __name__ == "__main__":
    main()
