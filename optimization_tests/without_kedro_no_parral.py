import time
import datetime
import re
import unicodedata
from fuzzywuzzy import fuzz
from typing import Any, Dict, Union
import dask.dataframe as dd
import multiprocessing
import string
from greeklish.converter import Converter
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.preprocessing.text import Tokenizer
import dateutil
import bios
import pickle
import tensorflow as tf
import yaml
from romanize import romanize
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
from kedro.extras.datasets.tensorflow import TensorFlowModelDataset
from tensorflow import argmax


def keep_description_classification(transactions_df):
    """ Keeps only description and classification of transactions """
    transactions_df["classification"] = (
        transactions_df["classification"] if "classification" in transactions_df.columns else None
    )

    return transactions_df[["description", "classification"]]


def remove_words(transactions_df: pd.DataFrame, words_df: pd.DataFrame) -> pd.DataFrame:
    """ 
    This function gets a transactions df and a dataframe of words. 
    It firstly changes descriptions to upper 
    It excludes the words from words df from each sentence as defined
    from names of selected columns
    """
    replace_words = re.compile(r"\b(" + ("|".join(words_df["word"])) + r")\b")
    transactions_df["processed_description"] = transactions_df["description"].apply(
        lambda row: (row.upper())
    )

    transactions_df["processed_description"] = transactions_df["processed_description"].str.replace(
        replace_words, ""
    )
    transactions_df.columns = ["initial_description", "classification", "processed_description"]

    return transactions_df


def clean_descriptions(transactions_df: pd.DataFrame) -> pd.DataFrame:
    """ 
    Combines all the basic cleaning functions. Also used the old one cleaning 
    function used for incoming transactions.

    Clean description function includes: clean_website_name, remove_punctuation, 
    connect_single_letters, remove_numbers, remove_single_letters

    Clean transaction descriptions includes: remove_accent_and_lowercase, 
    convert_characters
    """

    transactions_df["processed_description"] = transactions_df["processed_description"].apply(str)
    # Use the unique ones and then merge it in order to run quickly
    unique_descriptions_df = transactions_df.processed_description.drop_duplicates().reset_index(
        drop=True
    )
    processed_unique_descriptions_df = unique_descriptions_df.copy()
    processed_unique_descriptions_df = pd.DataFrame(processed_unique_descriptions_df)
    # Try to create one time a dask object and pass all the functions one by one
    processed_unique_descriptions_df["new_description"] = processed_unique_descriptions_df.apply(
        lambda row: clean_description(row["processed_description"]), axis=1
    )

    # Call the old function, as used also in incoming transactions
    cleaned_unique_descriptions_se = clean_transaction_descriptions(
        processed_unique_descriptions_df["new_description"]
    )
    # Replace the old column values with the new
    processed_unique_descriptions_df["new_description"] = cleaned_unique_descriptions_se

    cleaned_descriptions_df = pd.merge(
        transactions_df,
        processed_unique_descriptions_df,
        on=["processed_description", "processed_description"],
    )

    cleaned_descriptions_df = cleaned_descriptions_df[
        ["initial_description", "classification", "new_description"]
    ]
    cleaned_descriptions_df.columns = [
        "initial_description",
        "classification",
        "processed_description",
    ]

    return cleaned_descriptions_df


def clean_transaction_descriptions(descriptions_se: pd.Series) -> Union[pd.Series, pd.DataFrame]:
    cleaned_descriptions_se = descriptions_se.apply(lambda row: remove_accent_and_lowercase(row))
    cleaned_descriptions_se = cleaned_descriptions_se.apply(lambda row: convert_characters(row))

    return cleaned_descriptions_se


def convert_characters(description: str) -> str:
    """Converts the mixed letters of greek and english of a word to only english"""
    greek = False
    english = False
    new_description = ""
    for word in description.split():
        # TO DO: if it finds greek and english character has not to look other times
        for letter in word:

            if is_english(letter):
                english = True
            else:
                greek = True

        if (english == True) & (greek == True):
            greek_alphabet = "ΑαΒβΓγΔδΕεΖζΗηΘθΙιΚκΛλΜμΝνΞξΟοΠπΡρΣσςΤτΥυΦφΧχΨψΩω"
            new_alphabet = "AaBbGgDdEeZzHhJjIiKkLlMmVvXxOoPpPpSssTtYyFfXxYyWw"
            greek_to_new = str.maketrans(greek_alphabet, new_alphabet)

            new_description = new_description + " " + word.translate(greek_to_new)
        else:
            new_description = new_description + " " + word

        greek = False
        english = False

    return new_description[1:]


def is_english(character):
    """Checks if a character is english"""
    try:
        character.encode(encoding="utf-8").decode("ascii")
    except UnicodeDecodeError:
        return False
    else:
        return True


def remove_accent_and_lowercase(description: str) -> str:
    normalized_description = unicodedata.normalize("NFD", description)
    cleaned_description = re.sub("[\u0300-\u036f]", "", normalized_description).lower()

    return cleaned_description


def clean_description(description: str) -> str:
    """ Does the basic cleaning in a description"""
    cleaned_description = clean_website_name(description)
    cleaned_description = remove_punctuation(cleaned_description)
    cleaned_description = connect_single_letters(cleaned_description)
    cleaned_description = remove_numbers(cleaned_description)
    cleaned_description = remove_single_letters(cleaned_description)

    return cleaned_description


def clean_website_name(text: str) -> str:
    """ Example: "www.eshop.gr" is converted to "eshop" """
    list_of_words = []
    if text is not np.nan:
        for word in text.split():
            word = word.replace("www.", "")
            word = word.replace("WWW.", "")
            word = word.replace("WWW", "")
            word = word.replace("www", "")
            word = word.replace(".gr", "")
            word = word.replace(".GR", "")
            word = word.replace(".com", "")
            word = word.replace(".COM", "")
            list_of_words.append(word)
        words = " ".join(list_of_words)
    else:
        words = ""

    return words


def remove_punctuation(text: str) -> str:
    """ Removes punctations from descriptions """
    word_list = []
    for words in text:
        characters_list = []
        for characters in words:
            if characters not in string.punctuation:
                characters_list.append(characters)
            else:
                if characters == ".":
                    characters_list.append("")
                else:
                    characters_list.append(" ")
        word = "".join(characters_list)
        word_list.append(word)
    words = "".join(word_list)
    return words


def connect_single_letters(text: str) -> str:
    """ Example: "D E Y A  SITEIAS" is converted to "DEYA  SITEIAS" """
    pattern = re.compile(r"\b([a-z]) (?=[a-z]\b)", re.I)

    return re.sub(pattern, r"\g<1>", text)


def remove_numbers(text: str) -> str:
    """ Example: "FYSIKO AERIO VW507" is converted to "FYSIKO AERIO VW" """
    pattern = r"[0-9]"
    # Match all digits in the string and replace them by empty string
    modified_text = re.sub(pattern, "", text)

    return modified_text


def remove_single_letters(text: str) -> str:
    """ Removes the single letters included in descriptions """
    no_letter = [word for word in text.split() if len(word) > 1]
    words_no_letter = " ".join(no_letter)

    return words_no_letter


def convert_descriptions_to_english(transactions_df):
    """ 
    It converts descriptions to english. For example the greek words/sentences are coverted to greeklish.
    The english stay as it is.
    """

    # Use the unique ones and then merge it in order to run quickly
    unique_descriptions_df = (
        transactions_df["processed_description"].drop_duplicates().reset_index(drop=True)
    )
    processed_unique_descriptions_df = unique_descriptions_df.copy()
    processed_unique_descriptions_df = pd.DataFrame(processed_unique_descriptions_df)
    processed_unique_descriptions_df["new_description"] = processed_unique_descriptions_df.apply(
        lambda row: romanize_greek_to_latin(row["processed_description"]), axis=1
    )

    english_descriptions_df = pd.merge(
        transactions_df,
        processed_unique_descriptions_df,
        on=["processed_description", "processed_description"],
    )

    english_descriptions_df = english_descriptions_df[
        ["initial_description", "classification", "new_description"]
    ]
    english_descriptions_df.columns = [
        "initial_description",
        "classification",
        "processed_description",
    ]

    return english_descriptions_df


def romanize_greek_to_latin(description: str) -> str:
    """ 
    This function converts a greek description (every word) to latin through romanize function.
    Based on ISO 843:1997 standard (also known as ELOT 743:1987)
    """
    if description == "":
        return " "

    return romanize(description).lower()


def stem_english_descriptions(transactions_df):
    """ 
    It converts descriptions to english. For example the greek words/sentences are coverted to greeklish.
    The english stay as it is.
    """

    # Use the unique ones and then merge it in order to run quickly
    unique_descriptions_df = (
        transactions_df["processed_description"].drop_duplicates().reset_index(drop=True)
    )
    processed_unique_descriptions_df = unique_descriptions_df.copy()
    processed_unique_descriptions_df = pd.DataFrame(processed_unique_descriptions_df)

    processed_unique_descriptions_df["new_description"] = processed_unique_descriptions_df.apply(
        lambda row: english_stemmer(row["processed_description"]), axis=1
    )

    stemmed_descriptions_df = pd.merge(
        transactions_df,
        processed_unique_descriptions_df,
        on=["processed_description", "processed_description"],
    )

    stemmed_descriptions_df = stemmed_descriptions_df[
        ["initial_description", "classification", "new_description"]
    ]
    stemmed_descriptions_df.columns = [
        "initial_description",
        "classification",
        "description",
    ]

    return stemmed_descriptions_df


def english_stemmer(description):
    """ This function gets a description and returns the stemming of it (for each word of the description)"""
    stemmer = PorterStemmer()
    words = word_tokenize(description)
    list_of_words = []
    for w in words:
        transformed_word = stemmer.stem(w)
        transformed_word = transformed_word.lower()
        list_of_words.append(transformed_word)
    words = " ".join(list_of_words)

    return words


def get_descriptions_and_targets_from_transactions(
    transactions_df: pd.DataFrame,
) -> Dict[str, pd.Series]:
    transactions_df["classification"] = (
        transactions_df["classification"] if "classification" in transactions_df.columns else None
    )
    descriptions_se = transactions_df["description"]
    classifications_se = transactions_df["classification"]

    return dict(descriptions=descriptions_se, description_targets=classifications_se)


def pad_description_sequences(
    tokenizer: Tokenizer, descriptions_se: pd.Series, conf: Dict[str, int]
) -> np.ndarray:
    sequences = tokenizer.texts_to_sequences(descriptions_se.values)
    padded_sequences = pad_sequences(sequences, conf["max_sequence_length"])

    return padded_sequences


def predict_with_card_expenses_classifier(
    model,
    X: Union[pd.Series, pd.DataFrame],
    y_labels: list,
    other_category_threshold: float = 0.50,
):
    """Do predictions on given dataset."""
    if len(X) != 0:
        predictions = model.predict(X)
        prediction_labels_se = pd.Series([y_labels[argmax(p)] for p in predictions])
        confidence_se = pd.Series([p[argmax(p)] for p in predictions])

        return dict(
            predictions=pd.DataFrame(predictions),
            prediction_labels=prediction_labels_se,
            confidence_scores=confidence_se,
        )
    else:
        return dict(predictions=0, prediction_labels=pd.Series(0), confidence_scores=pd.Series(0))


def combine_transactions_results(
    transactions_df: pd.DataFrame,
    defined_class_transactions: pd.DataFrame,
    labels_df: pd.Series,
    confidence_scores: pd.Series,
    is_training: bool = False,
) -> pd.DataFrame:
    """ 
    Combines and merges the initial transactions with the new classification
    If is training = True (default) it results to two classification columns in order to campare them(the initial class
    and the new one given(predicted) class)
    If is training = False, it means we need only the classification so we drop the initial class, which will be actually
    None.
     """
    if not is_training:
        # Check if classification or/and confidence is in columns and drop them
        if "classification" in transactions_df.columns:
            transactions_df = transactions_df.drop(["classification"], axis=1)
        if "confidence" in transactions_df.columns:
            transactions_df = transactions_df.drop(["confidence"], axis=1)

    initial_description_df = defined_class_transactions[["initial_description"]]
    # TODO: check if this is the correct solution

    # Connect the predictions with descriptions in one dataframe
    predicted_transactions = pd.concat(
        [initial_description_df, labels_df, confidence_scores], axis=1
    )
    predicted_transactions = predicted_transactions.reset_index(drop=True)
    predicted_transactions.columns = ["description", "classification", "confidence"]
    # Create the unique rows dataframe and merge it with the initial to give the predcitions in classes
    unique_predicted_transactions = predicted_transactions.drop_duplicates(
        subset="description"
    ).reset_index(drop=True)

    final_transactions_df = pd.merge(
        transactions_df, unique_predicted_transactions, on=["description", "description"],
    )

    return final_transactions_df


def run_classification(transactions):
    parameters = bios.read("conf/base/parameters.yml")
    selected_card_expenses_transactions = keep_description_classification(transactions)
    card_expenses_words_to_remove = pd.read_csv(
        "data/04_feature/bag_of_words/card_expenses_words_to_remove.csv"
    )

    removed_words_non_mcc_card_transactions = remove_words(
        selected_card_expenses_transactions, card_expenses_words_to_remove
    )
    cleaned_non_mcc_card_descriptions = clean_descriptions(removed_words_non_mcc_card_transactions)
    english_converted_transactions = convert_descriptions_to_english(
        cleaned_non_mcc_card_descriptions
    )
    stemmed_english_transactions = stem_english_descriptions(english_converted_transactions)

    dict_des_tragets = get_descriptions_and_targets_from_transactions(stemmed_english_transactions)
    descriptions = dict_des_tragets["descriptions"]
    description_targets = dict_des_tragets["description_targets"]

    with open(
        "data/06_models/card_expenses_description_tokenizer/card_expenses_description_tokenizer.pickle",
        "rb",
    ) as handle:
        card_expenses_description_tokenizer = pickle.load(handle)

    card_expenses_description_classifier_model_params = parameters[
        "card_expenses_description_classifier_model_params"
    ]
    non_mcc_card_descriptions_as_padded_sequences = pad_description_sequences(
        card_expenses_description_tokenizer,
        descriptions,
        card_expenses_description_classifier_model_params,
    )

    # card_expenses_description_classifier_model = tf.saved_model.load(
    # "../data/06_models/card_expenses_description_classifier_model", tags=None, options=None
    # )
    model_data_set = TensorFlowModelDataset(
        "data/06_models/card_expenses_description_classifier_model"
    )
    card_expenses_description_classifier_model = model_data_set.load()
    a_yaml_file = open("data/06_models/labels/card_expenses_description_classifier_y_labels.yaml")
    card_expenses_description_classifier_y_labels = yaml.load(a_yaml_file, Loader=yaml.FullLoader)

    pred_dict = predict_with_card_expenses_classifier(
        card_expenses_description_classifier_model,
        non_mcc_card_descriptions_as_padded_sequences,
        card_expenses_description_classifier_y_labels,
    )

    card_expenses_description_predictions = pred_dict["predictions"]
    card_expenses_description_prediction_labels = pred_dict["prediction_labels"]
    non_mcc_card_descriptions_confidence_scores = pred_dict["confidence_scores"]

    non_mcc_card_expenses_output_transactions = combine_transactions_results(
        transactions,
        stemmed_english_transactions,
        card_expenses_description_prediction_labels,
        non_mcc_card_descriptions_confidence_scores,
    )

    return non_mcc_card_expenses_output_transactions


transactions = pd.read_csv("data/01_raw/bank/non_mcc_card_expenses_transactions.csv")
transactions_1 = transactions.drop_duplicates()[0:200].reset_index(drop=True)
transactions_2 = pd.read_csv("data/02_intermediate/splitted_non_mcc_card_expenses_transactions.csv")
transactions_3 = transactions.drop_duplicates()[0:1000].reset_index(drop=True)


start = time.time()
run_classification(transactions_1)
end = time.time()
print("Run for ", len(transactions_1), " rows for: ", end - start, " without kedro")

start = time.time()
run_classification(transactions_2)
end = time.time()
print("Run for ", len(transactions_2), " rows for: ", end - start, " without kedro")

start = time.time()
run_classification(transactions_3)
end = time.time()
print("Run for ", len(transactions_3), " rows for: ", end - start, " without kedro")

