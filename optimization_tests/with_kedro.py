import pandas as pd
import time
import numpy as np
from kedro.runner import SequentialRunner
from kedro.io import DataCatalog, MemoryDataSet
from kedro.extras.datasets.tensorflow import TensorFlowModelDataset
from kedro.extras.datasets.yaml import YAMLDataSet
from kedro.extras.datasets.pandas import CSVDataSet
from kedro.extras.datasets.pickle import PickleDataSet
import bios
import os
import sys

# Set up paths
module_path = os.path.abspath(os.path.join("../wyre"))
if module_path not in sys.path:
    sys.path.append(module_path)

from src.wyre.composers.descriptions import create_classify_non_mcc_card_descriptions_pipeline


transactions = pd.read_csv("data/01_raw/bank/non_mcc_card_expenses_transactions.csv")
transactions_1 = transactions.drop_duplicates()[0:200].reset_index(drop=True)
transactions_2 = pd.read_csv("data/02_intermediate/splitted_non_mcc_card_expenses_transactions.csv")
transactions_3 = transactions.drop_duplicates()[0:1000].reset_index(drop=True)


def data_catalog(non_mcc_card_expenses_transactions: pd.DataFrame):
    parameters = bios.read("conf/base/parameters.yml")
    return DataCatalog(
        {
            "non_mcc_card_expenses_transactions": MemoryDataSet(non_mcc_card_expenses_transactions),
            "card_expenses_words_to_remove": CSVDataSet(
                "data/04_feature/bag_of_words/card_expenses_words_to_remove.csv"
            ),
            "card_expenses_description_tokenizer": PickleDataSet(
                "data/06_models/card_expenses_description_tokenizer/card_expenses_description_tokenizer.pickle"
            ),
            "params:card_expenses_description_classifier_model_params": MemoryDataSet(
                parameters["card_expenses_description_classifier_model_params"]
            ),
            "card_expenses_description_classifier_model": TensorFlowModelDataset(
                "data/06_models/card_expenses_description_classifier_model"
            ),
            "card_expenses_description_classifier_y_labels": YAMLDataSet(
                "data/06_models/labels/card_expenses_description_classifier_y_labels.yaml"
            ),
        }
    )


runner = SequentialRunner()


def main():
    start = time.time()
    catalog = data_catalog(transactions_1)
    # Create pipeline
    pipeline = create_classify_non_mcc_card_descriptions_pipeline()
    # Run pipeline
    runner.run(pipeline, catalog)
    end = time.time()
    print("Run for ", len(transactions_1), " rows for: ", end - start, " with kedro")

    start = time.time()
    catalog = data_catalog(transactions_2)
    # Create pipeline
    pipeline = create_classify_non_mcc_card_descriptions_pipeline()
    # Run pipeline
    runner.run(pipeline, catalog)
    end = time.time()
    print("Run for ", len(transactions_2), " rows for: ", end - start, " with kedro")

    start = time.time()
    catalog = data_catalog(transactions_3)
    # Create pipeline
    pipeline = create_classify_non_mcc_card_descriptions_pipeline()
    # Run pipeline
    runner.run(pipeline, catalog)
    end = time.time()
    print("Run for ", len(transactions_3), " rows for: ", end - start, " with kedro")


if __name__ == "__main__":
    main()
