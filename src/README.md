|                     |                                                                                     |
| ------------------- | ----------------------------------------------------------------------------------- |
| Folder in wyre      | Description                                                                         |
| 01 Data engineering | Preprocessing / cleaning / normalizing of initial data. Work with data layers 01-05 |
| 02 Data science     | Model engineering (ML models, NNs, etc). Work with data layers 05-07                |
| 03 Data analytics   | Analytics for presentation purposes. Work with data layers 01-08                    |
