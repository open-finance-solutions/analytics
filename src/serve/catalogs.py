from pathlib import Path
from typing import Any
from kedro.io import DataCatalog, MemoryDataSet
from kedro.extras.datasets.tensorflow import TensorFlowModelDataset
from kedro.extras.datasets.yaml import YAMLDataSet
from kedro.extras.datasets.pandas import CSVDataSet
from kedro.extras.datasets.pickle import PickleDataSet
import pandas as pd
import bios


def get_data_catalog(
    input_transactions: pd.DataFrame, input_accounts: pd.DataFrame,
) -> DataCatalog:
    parameters = bios.read("conf/base/parameters.yml")
    return DataCatalog(
        {
            # MemoryDataSet
            "input_accounts": MemoryDataSet(input_accounts),
            "input_transactions": MemoryDataSet(input_transactions),
            "params:incoming_description_classifier_model_params": MemoryDataSet(
                parameters["incoming_description_classifier_model_params"]
            ),
            "params:is_income": MemoryDataSet(parameters["is_income"]),
            "params:is_expense": MemoryDataSet(parameters["is_expense"]),
            "params:is_training": MemoryDataSet(True),
            "params:card_expenses_description_classifier_model_params": MemoryDataSet(
                parameters["card_expenses_description_classifier_model_params"]
            ),
            "params:remittance_expenses_description_classifier_model_params": MemoryDataSet(
                parameters["remittance_expenses_description_classifier_model_params"]
            ),
            "params:banks_to_change": MemoryDataSet(parameters["banks_to_change"]),
            "params:alpha_bank_name": MemoryDataSet(parameters["alpha_bank_name"]),
            "params:nbg_bank_name": MemoryDataSet(parameters["nbg_bank_name"]),
            "params:other_category_threshold": MemoryDataSet(
                parameters["other_category_threshold"]
            ),
            # TensorFlowModelDataset
            "incoming_description_classifier_model": TensorFlowModelDataset(
                "data/06_models/incoming_description_classifier_model"
            ),
            "transaction_classifier_model": TensorFlowModelDataset(
                "data/06_models/transaction_classifier_model"
            ),
            "card_expenses_description_classifier_model": TensorFlowModelDataset(
                "data/06_models/card_expenses_description_classifier_model"
            ),
            "remittance_expenses_description_classifier_model": TensorFlowModelDataset(
                "data/06_models/remittance_expenses_description_classifier_model"
            ),
            # PickleDataSet
            "incoming_description_tokenizer": PickleDataSet(
                "data/06_models/incoming_description_tokenizer/incoming_description_tokenizer.pickle"
            ),
            # YAMLDataSet
            "incoming_description_classifier_y_labels": YAMLDataSet(
                "data/06_models/labels/incoming_description_classifier_y_labels.yaml"
            ),
            "transaction_classifier_y_labels": YAMLDataSet(
                "data/05_model_input/transaction_classifier_model/transaction_classifier_y_labels.yaml"
            ),
            "card_expenses_description_classifier_y_labels": YAMLDataSet(
                "data/06_models/labels/card_expenses_description_classifier_y_labels.yaml"
            ),
            "remittance_expenses_description_classifier_y_labels": YAMLDataSet(
                "data/06_models/labels/remittance_expenses_description_classifier_y_labels.yaml"
            ),
            # CSVDataSet
            "expenses_categories_bucket": CSVDataSet(
                "data/04_feature/bag_of_words/expenses_categories_bucket.csv"
            ),
            "essential_lifestyle_wysely_categories": CSVDataSet(
                "data/04_feature/bag_of_words/essential_lifestyle_wysely_categories.csv"
            ),
            "bank_digits": CSVDataSet(
                "data/04_feature/bag_of_words/bank_digits.csv", load_args=dict(dtype=object)
            ),
            "mcc_codes_to_wysely_categories": CSVDataSet(
                "data/04_feature/bag_of_words/mcc_codes_to_wysely_categories.csv"
            ),
            "card_expenses_description_tokenizer": PickleDataSet(
                "data/06_models/card_expenses_description_tokenizer/card_expenses_description_tokenizer.pickle"
            ),
            "card_expenses_words_to_remove": CSVDataSet(
                "data/04_feature/bag_of_words/card_expenses_words_to_remove.csv"
            ),
            "remittance_expenses_description_tokenizer": PickleDataSet(
                "data/06_models/remittance_expenses_description_tokenizer/remittance_expenses_description_tokenizer.pickle"
            ),
        }
    )


def get_income_training_catalog(training_incoming_transactions: pd.DataFrame) -> DataCatalog:
    parameters = bios.read("conf/base/parameters.yml")
    return DataCatalog(
        {
            "training_incoming_transactions": MemoryDataSet(training_incoming_transactions),
            "params:incoming_description_classifier_model_params": MemoryDataSet(
                parameters["incoming_description_classifier_model_params"],
            ),
            "params:is_training": MemoryDataSet(parameters["is_training"]),
            # TensorFlowModelDataset
            "incoming_description_classifier_model": TensorFlowModelDataset(
                "data/06_models/incoming_description_classifier_model"
            ),
            "card_expenses_words_to_remove": CSVDataSet(
                "data/04_feature/bag_of_words/card_expenses_words_to_remove.csv"
            ),
        }
    )
