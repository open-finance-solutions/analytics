from typing import Any
from fastapi.params import Body
from fastapi import FastAPI, Request
import pandas as pd
from kedro.runner import SequentialRunner
from src.serve.catalogs import get_data_catalog
import src.wyre.composers.classification as cl
from fastapi.responses import JSONResponse
import traceback
import time
import logging

log = logging.getLogger(__name__)

app = FastAPI()


async def catch_exceptions_middleware(request: Request, call_next):
    try:
        return await call_next(request)
    except Exception as e:
        tb = traceback.format_exc()
        print(tb)
        print("ERROR INFO: ", e)
        return JSONResponse(
            status_code=500, content={"detail": str(e), "meta": "Internal server error"}
        )


app.middleware("http")(catch_exceptions_middleware)


@app.get("/health")
def health_check():
    return {"status": "ok"}


@app.post("/report")
def report(data: Any = Body(...)):
    start = time.time()
    products = data["products"]
    accounts = data["accounts"]
    transactions = data["transactions"]

    accounts_df = pd.DataFrame(accounts)
    transactions_df = pd.DataFrame(transactions)

    runner = SequentialRunner(is_async=True)

    # Transactions
    classify_transactions_pipeline = cl.create_classify_transactions_full_pipeline()
    classify_transactions_catalog = get_data_catalog(transactions_df, input_accounts=accounts_df)
    classify_transactions_result = runner.run(
        classify_transactions_pipeline, classify_transactions_catalog
    )
    # Fillna in accounts dataframe for None values (needed for json conversion)
    transactions_output_transactions = classify_transactions_result[
        "full_classification_pipeline_output"
    ]  # .fillna(value=None, method=None, inplace=True)
    transactions_output_transactions = transactions_output_transactions.where(
        pd.notnull(transactions_output_transactions), None
    )
    accounts_df = accounts_df.where(pd.notnull(accounts_df), None)

    # Analytics
    analytics_pipeline = an.create_analytics_pipeline()
    analytics_catalog = get_data_catalog(
        transactions_output_transactions, input_accounts=accounts_df, input_products=products
    )
    analytics_result = runner.run(analytics_pipeline, analytics_catalog)
    end = time.time()
    log.info("Run time in seconds:")
    log.info(end - start)

    # print("Run for ", end - start, " seconds")
    return dict(
        report=analytics_result["report"],
        accounts=accounts_df.to_dict("records"),
        transactions=transactions_output_transactions.to_dict("records"),
    )


@app.post("/classify")
def classify_transactions(data: Any = Body(...)):
    accounts = data["accounts"]
    transactions = data["transactions"]

    accounts_df = pd.DataFrame(accounts)
    transactions_df = pd.DataFrame(transactions)

    runner = SequentialRunner(is_async=True)

    # Transactions
    classify_transactions_pipeline = cl.create_classify_transactions_full_pipeline()
    classify_transactions_catalog = get_data_catalog(transactions_df, input_accounts=accounts_df)
    classify_transactions_result = runner.run(
        classify_transactions_pipeline, classify_transactions_catalog
    )
    transactions_output_transactions = classify_transactions_result[
        "full_classification_pipeline_output"
    ].fillna(0)

    return dict(
        accounts=accounts_df.to_dict("records"),
        transactions=transactions_output_transactions.to_dict("records"),
    )
