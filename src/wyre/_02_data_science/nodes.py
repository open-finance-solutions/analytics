from typing import Dict, Union
import datetime
import pandas as pd
from tensorflow import argmax
from tensorflow.keras.layers import LSTM, Dense, Dropout, Embedding, SpatialDropout1D
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import SGD

from src.wyre._01_data_engineering.nodes import (
    remove_accent_and_uppercase,
    convert_characters,
    calculate_bounded_dates,
    check_similarity,
)


def train_description_classifier_model(
    X_train: Union[pd.Series, pd.DataFrame],
    y_train: Union[pd.Series, pd.DataFrame],
    y_labels: list,
    conf: Dict[str, int],
):
    """Train model on given inputs. Currently is used for remittance incoming and expenses """
    model = Sequential()
    model.add(
        Embedding(conf["max_words"], conf["embedding_dimensions"], input_length=X_train.shape[1],)
    )
    model.add(SpatialDropout1D(0.2))
    model.add(LSTM(100, dropout=0.2, recurrent_dropout=0.2))
    model.add(Dense(len(y_labels), activation="softmax"))

    model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])

    history = model.fit(
        X_train.to_numpy(),
        y_train.to_numpy(),
        validation_split=0.2,
        batch_size=conf["batch_size"],
        epochs=conf["epochs"],
    )

    return model


def train_transaction_classifier_model(
    X_train: Union[pd.Series, pd.DataFrame],
    y_train: Union[pd.Series, pd.DataFrame],
    y_labels: list,
    conf: Dict[str, int],
):
    input_length = X_train.shape[1]

    model = Sequential()
    model.add(Dense(input_length, activation="relu", input_shape=(input_length,)))
    model.add(Dense(64, activation="relu"))
    model.add(Dense(64, activation="relu"))
    model.add(Dropout(0.1))
    model.add(Dense(len(y_labels), activation="softmax"))

    model.compile(
        optimizer=SGD(learning_rate=0.01),
        loss="sparse_categorical_crossentropy",
        metrics=["accuracy"],
    )

    history = model.fit(
        X_train.to_numpy(),
        y_train.to_numpy(),
        validation_split=0.2,
        batch_size=conf["batch_size"],
        epochs=conf["epochs"],
    )

    return model


def evaluate_model(
    model, X_test: Union[pd.Series, pd.DataFrame], y_test: Union[pd.Series, pd.DataFrame],
):
    """Evaluates the model calculating accuracy and loss."""
    evaluation = model.evaluate(X_test.to_numpy(), y_test.to_numpy())

    return dict(accuracy=evaluation[1], loss=evaluation[0])


def predict_with_classifier(model: Sequential, X: Union[pd.Series, pd.DataFrame], y_labels: list):
    """Do predictions on given dataset."""
    if len(X) != 0:
        predictions = model.predict(X)
        prediction_labels_se = pd.Series([y_labels[argmax(p)] for p in predictions])
        confidence_se = pd.Series([p[argmax(p)] for p in predictions])
        return dict(
            predictions=pd.DataFrame(predictions),
            prediction_labels=prediction_labels_se,
            confidence_scores=confidence_se,
        )
    else:
        return dict(predictions=0, prediction_labels=pd.Series(0), confidence_scores=pd.Series(0))


def classify_description_from_bow(
    description: str, bucket: pd.DataFrame, default_class="expenditure"
) -> str:
    corrected_characters_description = convert_characters(description)
    for i, row in bucket.iterrows():
        if row["description"] in remove_accent_and_uppercase(corrected_characters_description):
            classification = row["category"]
            return classification

    # If nothing is found it should be the default class
    classification = default_class
    return classification


def classify_transactions(transactions_df: pd.DataFrame, bucket: pd.DataFrame) -> pd.DataFrame:
    transactions_df["classification"] = transactions_df.apply(
        lambda row: classify_description_from_bow(row["description"], bucket), axis=1
    )
    return transactions_df


def classify_salaries_programmatically(transactions_df: pd.DataFrame):
    """
    Salary transaction acceptance criteria:
        - Month has no salary transaction
        - Transfers more than 50% of the transactions of specific month
        - Transfers found on the same date (+- 3 days) in more than 3 months
    Confidence score of found salaries is 0.85 hardcoded. The amount
    variation of the transactions we are searching is +- 5%
    """
    transactions_df["date"] = pd.to_datetime(transactions_df["date"])
    months_se = transactions_df["date"].map(lambda x: x.month)
    transactions_df["months"] = months_se

    # Month has no salary transaction
    months_with_no_identified_salaries_df_list = []
    for i in range(1, 13):
        filtered_df = transactions_df.query(f"months == {i} & classification == 'salary'")
        if len(filtered_df) == 0:
            month_df = transactions_df.query(f"months == {i}")
            months_with_no_identified_salaries_df_list.append(month_df)

    # Transfers more than 50% of the transactions of specific month
    transactions_more_than_50_perc_df = pd.DataFrame()
    for month_df in months_with_no_identified_salaries_df_list:
        total_month_income = month_df["amount"].sum()
        query = f"amount > {total_month_income / 2} and classification == 'transfer'"
        tranactions_more_than_50_perc_df = month_df.query(query)
        if len(tranactions_more_than_50_perc_df):
            max = tranactions_more_than_50_perc_df["amount"].max()
            highest_transaction_df = tranactions_more_than_50_perc_df.query(f"amount == {max}")
            transactions_more_than_50_perc_df = transactions_more_than_50_perc_df.append(
                highest_transaction_df
            )

    # Is found on the same date (+- 3 days) in more than 3 months
    salaries_found = []
    for index, row in transactions_more_than_50_perc_df.iterrows():
        # Complex querying at its finest
        upper_bound_date = row["date"] + datetime.timedelta(days=3)
        lower_bound_date = row["date"] - datetime.timedelta(days=3)
        dates_query = f"date <= '{upper_bound_date.date()}' or date >= '{lower_bound_date.date()}'"
        not_same_date_query = f"date != '{row['date'].date()}'"
        amount_query = f"amount <= {row['amount'] * 1.05} and amount >= {row['amount'] * 0.95}"
        # Print next line to make sense of the above
        final_query = f"({amount_query}) and ({not_same_date_query} and ({dates_query}))"
        similar_df = transactions_more_than_50_perc_df.query(final_query)
        if len(similar_df["date"]) > 3:
            salaries_found.append(row.to_dict())

    # Change all found description classifications
    def sal(t):
        found = (
            bool(s) for s in salaries_found if t["amount"] == s["amount"] and t["date"] == s["date"]
        )
        is_salary = next(found, None)
        if is_salary:
            t["classification"] = "salary"
        return t

    transactions_df = transactions_df.apply(sal, axis=1)
    transactions_df["confidence"] = 0.85
    return dict(
        prediction_labels=transactions_df["classification"],
        confidence_scores=transactions_df["confidence"],
    )


def classify_reccuring_payments_programmatically(
    transactions_df: pd.DataFrame, amount_percentage=0.05, description_threshold=0.70
):
    """
    Classify reccuring payments of 3 or more months.
    It is based on dates, amounts and description similarity
    """
    # Is found on the same date (+- 3 days) in more than 3 months
    reccuring_transactions_df = pd.DataFrame()
    transactions_df["date"] = pd.to_datetime(transactions_df["date"])
    for index, row in transactions_df.iterrows():
        # Create bounded dates
        upper_bound_date_min, upper_bound_date_max = calculate_bounded_dates(row["date"], 1)
        lower_bound_date_min, lower_bound_date_max = calculate_bounded_dates(row["date"], -1)
        # Create query for the current date of row
        same_date_query = f"date == '{row['date'].date()}'"
        # Create queries for one month ahead and one month back with some extra free days
        upper_date_query = (
            f"date <= '{upper_bound_date_max.date()}' & date >= '{upper_bound_date_min.date()}'"
        )
        lower_date_query = (
            f"date >= '{lower_bound_date_min.date()}' & date <= '{lower_bound_date_max.date()}'"
        )
        dates_query = f"{upper_date_query} | {lower_date_query}"
        # Create query for same amount with some small variation
        amount_query = f"amount <= {row['amount'] * (1+amount_percentage)} and amount >= {row['amount'] * (1-amount_percentage)}"
        # Connect all the above queries
        final_query = f"({amount_query}) & ({dates_query} | {same_date_query})"
        # The final query returns a dataframe
        similar_df = transactions_df.query(final_query).reset_index(drop=True)
        # Check the similarity of descriptions into the created dataframe. We want to keep the same
        similar_description_df = check_similarity(
            similar_df, row["description"], description_threshold
        )
        # If len is equal or more than 3, it means we have the same criteria for more than 3 months in order
        # to characterize a transaction as recurring. Append all the recurring found each time
        if len(similar_description_df) >= 3:
            reccuring_transactions_df = reccuring_transactions_df.append(
                similar_description_df, ignore_index=True
            )

    # Consider all transactions as non recurring
    transactions_df["is_recurring"] = False
    if len(reccuring_transactions_df):
        # Some of the transactions may have appended more than one times depending on their position
        # So drop the duplicated transactions
        unique_reccuring_transactions_df = reccuring_transactions_df.drop_duplicates().reset_index(
            drop=True
        )
        # Find common positions in transactions and recurring transactions
        common_condition = transactions_df["provider_id"].isin(
            unique_reccuring_transactions_df["provider_id"]
        )
        # Find non recurring transactions
        non_recurring_transactions_df = transactions_df[~common_condition]
        # The recurring transactions are True
        unique_reccuring_transactions_df["is_recurring"] = True
        # Combine them all in one dataframe
        transactions_df = unique_reccuring_transactions_df.append(
            non_recurring_transactions_df, ignore_index=True
        )

    return transactions_df


def find_wysely_category_from_mcc(
    mcc: str, mcc_codes_to_wysely_categories_df: pd.DataFrame, no_mcc_category: str = "other"
) -> str:
    """
    Based on mcc code, it finds the wysely category based on mcc_codes_to_wysely_categories file
    no_mcc_category will return other in mcc codes not found in mcc_codes_to_wysely_categories file
    """
    # TODO: Check if we want to pass the transactions that have mccs not included in file
    # back to neural network of card expenses to classify them or just leave it as 'other'

    if mcc in mcc_codes_to_wysely_categories_df["mcc"].values:
        temp_df = mcc_codes_to_wysely_categories_df.loc[
            mcc_codes_to_wysely_categories_df["mcc"] == mcc
        ]
        temp_df = temp_df.reset_index(drop=True)

        return temp_df["wysely_category"][0]
    else:
        return no_mcc_category


def classify_transactions_on_mcc(
    transactions_df: pd.DataFrame, mcc_codes_to_wysely_categories_df: pd.DataFrame
) -> pd.DataFrame:
    """
    Takes as input a transactions dataframe and an mcc to wysely categories dataframe and
    classifies each transaction
    """
    if not len(transactions_df):
        return transactions_df

    # Be sure that the mcc come as an str type
    transactions_df["mcc"] = transactions_df["mcc"].astype(str)
    transactions_df["classification"] = transactions_df.apply(
        lambda x: find_wysely_category_from_mcc(x["mcc"], mcc_codes_to_wysely_categories_df), axis=1
    )

    transactions_df["confidence"] = 0.99
    return transactions_df
