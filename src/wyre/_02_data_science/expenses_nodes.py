import dask.dataframe as dd
import multiprocessing
import pandas as pd
import numpy as np
import datetime
import string
import re
import unicodedata
from greeklish.converter import Converter
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
from romanize import romanize

from typing import Dict, Union
from tensorflow import argmax
from tensorflow.keras.layers import (
    LSTM,
    Conv1D,
    MaxPooling1D,
    Dense,
    Dropout,
    Embedding,
    SpatialDropout1D,
)
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import SGD

from src.wyre._01_data_engineering.nodes import (
    clean_transaction_descriptions,
    reclassify_duplicated_remittance_expenses,
)


def split_transactions_on_duplicated(transactions_df):
    """ Get duplicated and not duplicated transactions based on same descriptions but differenrent mcc or mcg names"""

    # Keep unique values based on both description and  other column
    unique_transactions_df = transactions_df.drop_duplicates().reset_index(drop=True)
    # Get from the entire unique (both description and other included columns)
    # the positions of duplicated based on description
    duplicated_descriptions_positions = unique_transactions_df.duplicated(
        keep=False, subset="description"
    )
    # So these will contain the same descriptions that have differnet categories
    # Keep the non duplicated
    non_duplicated = unique_transactions_df[~duplicated_descriptions_positions]
    non_duplicated = non_duplicated.reset_index(drop=True)
    # Keep the duplicated
    duplicated = unique_transactions_df[duplicated_descriptions_positions]
    duplicated = duplicated.reset_index(drop=True)

    return duplicated, non_duplicated


def correct_duplicated_categories(transactions_df, manual_df):
    """
    Gets descriptions, descriptions_targets and specified classification. It finds the duplicated descriptions
    that fall under different classification and converts their classification to "other". If these descriptions
    are included in the second dataframe manual_df, the new classification comes from this. A duplicated dataframe
    is being created each time with, in order to check it and manually put some of the descriptions to manual file
    """

    transactions_to_check_df = transactions_df[["description", "classification"]]
    duplicated, non_duplicated = split_transactions_on_duplicated(transactions_to_check_df)
    adjusted_manual_df = manual_df.drop_duplicates()
    adjusted_manual_df = adjusted_manual_df.reset_index(drop=True)

    # Have everything of the duplicated as other and then correct the manual corrected with the right category as defined.
    # In the future we can come back here and manually check and adjust more obvious categories in order to have better results
    duplicated.classification = "other"
    duplicated = duplicated.drop_duplicates()
    duplicated = duplicated.reset_index(drop=True)

    # The 2 above dataframes include unique values. After merging them, check for the same descriptions
    # After the merge a new column "exist" will be created. If a value is in both dataframes will get "both" value
    merged_descriptions_df = pd.merge(
        duplicated,
        adjusted_manual_df,
        on=["description", "description"],
        how="left",
        indicator="Exist",
    )
    merged_descriptions_df["Exist"] = np.where(merged_descriptions_df.Exist == "both", True, False)

    # Convert to str values beacuse some nan values were created
    merged_descriptions_df["classification_y"] = merged_descriptions_df["classification_y"].astype(
        str
    )
    merged_descriptions_df["classification_x"] = merged_descriptions_df["classification_x"].astype(
        str
    )
    # If classification of manual dataframe is nan give the classification of the first which is "other",
    # else classification of the second one (manual)
    merged_descriptions_df["classification"] = merged_descriptions_df["classification_x"].where(
        merged_descriptions_df["classification_y"] == "nan",
        merged_descriptions_df["classification_y"],
    )
    # Keep the 2 columns
    merged_descriptions_df = merged_descriptions_df[["description", "classification"]]

    # Combine the new dataframe with the non duplicated
    combined_dupl_df = merged_descriptions_df.append(non_duplicated, ignore_index=True)
    # Keep only description from basic transactions and then get the classification from the merge with the new one
    defined_class_transactions_df = transactions_df[["initial_description", "description"]]
    defined_class_transactions_df = pd.merge(
        defined_class_transactions_df, combined_dupl_df, on=["description", "description"],
    )
    # Keep the duplicated in order to check the csv for checking the duplicated values (which will be in other category)
    return dict(
        defined_class_transactions=defined_class_transactions_df,
        duplicated_class_card_expenses=duplicated,
    )


def romanize_greek_to_latin(description):
    """
    This function converts a greek description (every word) to latin through romanize function.
    Based on ISO 843:1997 standard (also known as ELOT 743:1987)
    """
    if description == "":
        return " "

    return romanize(description).lower()


def english_stemmer(description):
    """ This function gets a description and returns the stemming of it (for each word of the description)"""
    stemmer = PorterStemmer()
    words = word_tokenize(description)
    list_of_words = []
    for w in words:
        transformed_word = stemmer.stem(w)
        transformed_word = transformed_word.lower()
        list_of_words.append(transformed_word)
    words = " ".join(list_of_words)

    return words


def convert_descriptions_to_english(transactions_df: pd.DataFrame, is_training: bool = False):
    """
    It converts descriptions to english. For example the greek words/sentences are coverted to greeklish.
    The english stay as it is.
    """

    # Use the unique ones and then merge it in order to run quickly
    unique_descriptions_df = (
        transactions_df["processed_description"].drop_duplicates().reset_index(drop=True)
    )
    processed_unique_descriptions_df = unique_descriptions_df.copy()
    processed_unique_descriptions_df = pd.DataFrame(processed_unique_descriptions_df)

    # If is training use dask otherwise use pandas
    if is_training:
        processed_unique_descriptions_df["new_description"] = (
            dd.from_pandas(
                processed_unique_descriptions_df["processed_description"],
                npartitions=4 * multiprocessing.cpu_count(),
            )
            .map_partitions(lambda dframe: dframe.apply(lambda row: romanize_greek_to_latin(row)))
            .compute(scheduler="processes")
        )
    else:
        processed_unique_descriptions_df[
            "new_description"
        ] = processed_unique_descriptions_df.apply(
            lambda row: romanize_greek_to_latin(row["processed_description"]), axis=1
        )

    english_descriptions_df = pd.merge(
        transactions_df,
        processed_unique_descriptions_df,
        on=["processed_description", "processed_description"],
    )

    english_descriptions_df = english_descriptions_df[
        ["initial_description", "classification", "new_description"]
    ]
    english_descriptions_df.columns = [
        "initial_description",
        "classification",
        "processed_description",
    ]

    return english_descriptions_df


def stem_english_descriptions(transactions_df: pd.DataFrame, is_training: bool = False):
    """
    It converts descriptions to english. For example the greek words/sentences are coverted to greeklish.
    The english stay as it is.
    """

    # Use the unique ones and then merge it in order to run quickly
    unique_descriptions_df = (
        transactions_df["processed_description"].drop_duplicates().reset_index(drop=True)
    )
    processed_unique_descriptions_df = unique_descriptions_df.copy()
    processed_unique_descriptions_df = pd.DataFrame(processed_unique_descriptions_df)

    # If is training use dask, otherwise use pandas
    if is_training:
        processed_unique_descriptions_df["new_description"] = (
            dd.from_pandas(
                processed_unique_descriptions_df["processed_description"],
                npartitions=4 * multiprocessing.cpu_count(),
            )
            .map_partitions(lambda dframe: dframe.apply(lambda row: english_stemmer(row)))
            .compute(scheduler="processes")
        )
    else:
        processed_unique_descriptions_df[
            "new_description"
        ] = processed_unique_descriptions_df.apply(
            lambda row: english_stemmer(row["processed_description"]), axis=1
        )

    stemmed_descriptions_df = pd.merge(
        transactions_df,
        processed_unique_descriptions_df,
        on=["processed_description", "processed_description"],
    )

    stemmed_descriptions_df = stemmed_descriptions_df[
        ["initial_description", "classification", "new_description"]
    ]
    stemmed_descriptions_df.columns = [
        "initial_description",
        "classification",
        "description",
    ]

    return stemmed_descriptions_df


def train_expenses_description_classifier_model(
    X_train: Union[pd.Series, pd.DataFrame],
    y_train: Union[pd.Series, pd.DataFrame],
    y_labels: list,
    conf: Dict[str, int],
):
    """Train model on given inputs. Currently is used on card expenses descriptions """
    model = Sequential()
    model.add(
        Embedding(conf["max_words"], conf["embedding_dimensions"], input_length=X_train.shape[1],)
    )
    model.add(Conv1D(filters=32, kernel_size=3, padding="same", activation="relu"))
    model.add(MaxPooling1D(pool_size=2))
    # model.add(SpatialDropout1D(0.2))
    model.add(LSTM(100, dropout=0.2, recurrent_dropout=0.2))
    model.add(Dense(len(y_labels), activation="softmax"))

    model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])

    history = model.fit(
        X_train.to_numpy(),
        y_train.to_numpy(),
        validation_split=0.2,
        batch_size=conf["batch_size"],
        epochs=conf["epochs"],
    )

    return model


def predict_with_card_expenses_classifier(
    model: Sequential,
    X: Union[pd.Series, pd.DataFrame],
    y_labels: list,
    other_category_threshold: float = 0.50,
):
    """Do predictions on given dataset."""
    if len(X) != 0:
        predictions = model.predict(X)
        prediction_labels_se = pd.Series([y_labels[argmax(p)] for p in predictions])
        confidence_se = pd.Series([p[argmax(p)] for p in predictions])

        return dict(
            predictions=pd.DataFrame(predictions),
            prediction_labels=prediction_labels_se,
            confidence_scores=confidence_se,
        )
    else:
        return dict(predictions=0, prediction_labels=pd.Series(0), confidence_scores=pd.Series(0))


def combine_pipeline_results(
    incoming_classified_output_transactions: pd.DataFrame,
    final_exprenses_output_transactions: pd.DataFrame,
) -> pd.DataFrame:
    """ It combines final classified incoming transactions with final classified expenses transactions"""

    combined_df = pd.DataFrame()
    combined_df = combined_df.append(incoming_classified_output_transactions, ignore_index=True)
    combined_df = combined_df.append(final_exprenses_output_transactions, ignore_index=True)

    return combined_df


def combine_card_expenses(
    mcc_card_expenses_output_transactions: pd.DataFrame,
    non_mcc_card_expenses_output_transactions: pd.DataFrame,
) -> pd.DataFrame:
    """Combines mcc and non mcc card expenses"""

    card_expenses_df = mcc_card_expenses_output_transactions.append(
        non_mcc_card_expenses_output_transactions, ignore_index=True
    )

    return card_expenses_df.drop(["bank"], axis=1)


def clean_columns(transactions_df: pd.DataFrame,) -> pd.DataFrame:
    """Gets transactions and drops type column and renames initial type column to type"""

    # Drop the temp type, classification is done no needed. The ones coming from provider will remain
    # for the analytics part and is renamed back as type
    cleaned_transactions_df = transactions_df.drop(["type"], axis=1)

    return cleaned_transactions_df.rename(columns={"initial_type": "type"})


# TODO: Not used?
def combine_expenses_pipeline_results(
    card_expenses_output_transactions: pd.DataFrame,
    remittance_expenses_output_transactions: pd.DataFrame,
) -> pd.DataFrame:
    """
    It combines final expenses categories transactions (mcc card expenses, non mcc card expenses
    , remittance expenses). It also returns the initial type of the transactions.
    """

    # Combine them all together
    combined_df = pd.DataFrame()
    combined_df = combined_df.append(card_expenses_output_transactions, ignore_index=True)
    combined_df = combined_df.append(remittance_expenses_output_transactions, ignore_index=True)

    return combined_df


# TODO: not used. Maybe should be used for inner transfers?
def combine_card_remitance_expenses(
    card_expenses: pd.DataFrame, remittance_expenses: pd.DataFrame,
) -> pd.DataFrame:
    """
    It combines card expenses and remittance expenses
    """
    # Add column is inner in card expenses with False values
    card_expenses["is_inner"] = False
    # Combine them all together
    combined_df = pd.DataFrame()
    combined_df = combined_df.append(card_expenses, ignore_index=True)

    combined_df = combined_df.append(remittance_expenses, ignore_index=True)

    return combined_df
