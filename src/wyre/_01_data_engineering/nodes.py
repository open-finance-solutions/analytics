import datetime
import re
import unicodedata
from fuzzywuzzy import fuzz
from typing import Any, Dict, Union
import dask.dataframe as dd
import multiprocessing
import string
from greeklish.converter import Converter
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.preprocessing.text import Tokenizer
import dateutil


def find_common_transactions(
    incoming_transactions_df: pd.DataFrame, outgoing_transactions_df: pd.DataFrame, is_left
):
    """Takes two dataframes and returns the common rows of first if is_left=True or second if is_left=False
    this function is used by flag_inner_transfers function"""

    cut_incoming_transactions_df = incoming_transactions_df[["amount", "date"]]
    cut_outgoing_transactions_df = outgoing_transactions_df[["amount", "date"]]

    is_inner_df = cut_incoming_transactions_df.merge(
        cut_outgoing_transactions_df, on=["amount", "date"], how="inner"
    )
    is_inner_df = is_inner_df.drop_duplicates()

    if is_left:
        common_incoming_transactions_df = (
            incoming_transactions_df.reset_index()
            .merge(is_inner_df, on=["amount", "date"], how="inner")
            .set_index("index")
        )
        return common_incoming_transactions_df
    else:
        common_outgoing_transactions_df = (
            outgoing_transactions_df.reset_index()
            .merge(is_inner_df, on=["amount", "date"], how="inner")
            .set_index("index")
        )
        return common_outgoing_transactions_df


def flag_inner_transfers(
    incoming_transactions_df: pd.DataFrame, outgoing_transactions_df: pd.DataFrame
):
    """Create new column to identify inner_transfers between incoming and outgoing transactions"""

    # Create the new column to identify inner transfers
    incoming_transactions_df["is_inner"] = False
    outgoing_transactions_df["is_inner"] = False

    # Find common rows for each dataframe
    incoming_common_transactions_df = find_common_transactions(
        incoming_transactions_df, outgoing_transactions_df, True
    )
    outgoing_common_transactions_df = find_common_transactions(
        incoming_transactions_df, outgoing_transactions_df, False
    )

    # Compare each row of common incoming and outgoing
    for index, row in incoming_common_transactions_df.iterrows():
        for index_2, row_2 in outgoing_common_transactions_df.iterrows():
            # If has not been flagged as inner
            if row_2["is_inner"] == False:
                if (row["amount"] == row_2["amount"]) & (row["date"] == row_2["date"]):
                    incoming_transactions_df.at[index, "is_inner"] = True
                    outgoing_transactions_df.at[index_2, "is_inner"] = True
                    outgoing_common_transactions_df.at[index_2, "is_inner"] = True
                    # If it finds a same row, no reason to continue
                    break

    return dict(
        incoming_transactions=incoming_transactions_df.reset_index(drop=True),
        outgoing_transactions=outgoing_transactions_df.reset_index(drop=True),
    )


def filter_non_inner_transfers(transactions_df: pd.DataFrame) -> pd.DataFrame:

    return transactions_df.loc[transactions_df["is_inner"] == False].reset_index(drop=True)


def split_incoming_and_outgoing(transactions_df: pd.DataFrame) -> Dict[str, pd.DataFrame]:

    transactions_df["date"] = pd.to_datetime(transactions_df["date"])
    incoming_transactions_df = transactions_df[transactions_df["is_income"] == True].reset_index(
        drop=True
    )
    outgoing_transactions_df = transactions_df[transactions_df["is_income"] == False].reset_index(
        drop=True
    )

    return dict(
        incoming_transactions=incoming_transactions_df,
        outgoing_transactions=outgoing_transactions_df,
    )


def split_training_test_set(
    X: Union[pd.Series, pd.DataFrame], y: pd.Series, conf: Dict[str, int]
) -> Dict[str, Any]:
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=conf["test_size"], random_state=conf["random_state"]
    )

    return dict(
        X_train=pd.DataFrame(X_train),
        X_test=pd.DataFrame(X_test),
        y_train=pd.DataFrame(y_train),
        y_test=pd.DataFrame(y_test),
    )


def get_descriptions_and_targets_from_transactions(
    transactions_df: pd.DataFrame,
) -> Dict[str, pd.Series]:
    transactions_df["classification"] = (
        transactions_df["classification"] if "classification" in transactions_df.columns else None
    )
    descriptions_se = transactions_df["description"]
    classifications_se = transactions_df["classification"]

    return dict(descriptions=descriptions_se, description_targets=classifications_se)


def remove_accent_and_uppercase(description: str) -> str:
    normalized_description = unicodedata.normalize("NFD", description)
    cleaned_description = re.sub("[\u0300-\u036f]", "", normalized_description).upper()

    return cleaned_description


def remove_accent_and_lowercase(description: str) -> str:
    normalized_description = unicodedata.normalize("NFD", description)
    cleaned_description = re.sub("[\u0300-\u036f]", "", normalized_description).lower()

    return cleaned_description


# TODO: Check this function again. Many for loops
def convert_characters(description: str) -> str:
    """Converts the mixed letters of greek and english of a word to only english"""
    greek = False
    english = False
    new_description = ""
    for word in description.split():
        # TO DO: if it finds greek and english character has not to look other times
        for letter in word:

            if is_english(letter):
                english = True
            else:
                greek = True

        if (english == True) & (greek == True):
            greek_alphabet = "ΑαΒβΓγΔδΕεΖζΗηΘθΙιΚκΛλΜμΝνΞξΟοΠπΡρΣσςΤτΥυΦφΧχΨψΩω"
            new_alphabet = "AaBbGgDdEeZzHhJjIiKkLlMmVvXxOoPpPpSssTtYyFfXxYyWw"
            greek_to_new = str.maketrans(greek_alphabet, new_alphabet)

            new_description = new_description + " " + word.translate(greek_to_new)
        else:
            new_description = new_description + " " + word

        greek = False
        english = False

    return new_description[1:]


# TODO: Check this function again. Many for loops
def convert_alpha_characters(description: str) -> str:
    """Converts the mixed letters of greek and english of a word to only english"""
    greek = False
    english = False
    new_description = ""
    for word in description.split():
        for letter in word:
            if is_english(letter):
                english = True
            else:
                greek = True
            if (english == True) & (greek == True):
                # if found both greek and enlish letters break do not continue
                break
    if (english == True) & (greek == True):
        greek_alphabet = "ΑαΒβΓγΔδΕεΖζΗηΘθΙιΚκΛλΜμΝνΞξΟοΠπΡρΣσςΤτΥυΦφΧχΨψΩω"
        english_alphabet = "AaBbGgDdEeZzHhJjIiKkLlMmNnXxOoPpPpSssTtYyFfXxYyWw"
        transform = str.maketrans(greek_alphabet, english_alphabet)
        new_description = description.translate(transform)

        return new_description

    return description


def is_english(character):
    """Checks if a character is english"""
    try:
        character.encode(encoding="utf-8").decode("ascii")
    except UnicodeDecodeError:
        return False
    else:
        return True


def clean_transaction_descriptions(descriptions_se: pd.Series) -> Union[pd.Series, pd.DataFrame]:
    cleaned_descriptions_se = descriptions_se.apply(lambda row: remove_accent_and_lowercase(row))
    cleaned_descriptions_se = cleaned_descriptions_se.apply(lambda row: convert_characters(row))

    return cleaned_descriptions_se


def create_tokenizer(descriptions_se: pd.Series, conf: Dict[str, int]) -> Tokenizer:
    filters = '!"#$%&()*+,-./:;<=>?@[\]^_`{|}~'
    tokenizer = Tokenizer(num_words=conf["max_words"], filters=filters, lower=True)
    tokenizer.fit_on_texts(descriptions_se.values)
    return tokenizer


def pad_description_sequences(
    tokenizer: Tokenizer, descriptions_se: pd.Series, conf: Dict[str, int]
) -> np.ndarray:
    sequences = tokenizer.texts_to_sequences(descriptions_se.values)
    padded_sequences = pad_sequences(sequences, conf["max_sequence_length"])

    return padded_sequences


def encode_y_targets(y: pd.Series):
    y_dummies = pd.get_dummies(y)
    y_labels = y_dummies.columns.tolist()
    label_encoder = LabelEncoder()
    label_transformed = label_encoder.fit_transform(y)
    y_enc = pd.Series(label_transformed)

    return dict(y_enc=y_enc.values, y_labels=y_labels)


def dummify_y_targets(y: pd.Series):
    y_dummies = pd.get_dummies(y)
    y_labels = y_dummies.columns.tolist()

    return dict(y_dummies=y_dummies.values, y_labels=y_labels)


def connect_dataframes(df1, df2, connect):
    """
    It gets two dataframes and connects them to one, if connect=True, otherwise
    it returns the first one
    """
    if connect["connect_transactions"]:
        return df1.append(df2, ignore_index=True)
    else:
        return df1


def calculate_class_frequency(transactions_df: pd.DataFrame, months: int = 12) -> pd.Series:
    if not "customer_id" in transactions_df:
        transactions_df["customer_id"] = "1"

    counts_per_customer = dict()
    for id in transactions_df["customer_id"].unique():
        counts = (
            transactions_df[transactions_df["customer_id"] == id]["classification"]
            .value_counts()
            .to_dict()
        )
        counts_per_customer[id] = counts

    frequency_se = transactions_df.apply(
        lambda row: counts_per_customer[row["customer_id"]][row["classification"]] / months, axis=1,
    )
    return frequency_se


def calculate_month_last_business_day(transactions_df: pd.DataFrame) -> pd.Series:
    def is_last_business_day(date):
        date = datetime.datetime.strptime(date.strftime("%Y-%m-%d"), "%Y-%m-%d")
        possible_last_month_days = [28, 29, 30, 31]

        month_start = date.replace(day=1)
        month_end = None  # Calculated below
        for last_day in possible_last_month_days:
            try:
                month_end = date.replace(day=last_day)
            except:
                pass

        month_business_days = pd.bdate_range(month_start, month_end)
        return month_business_days[-1].strftime("%Y-%m-%d") == date.strftime("%Y-%m-%d")

    is_last_business_day_se = transactions_df.apply(
        lambda row: is_last_business_day(row["date"]), axis=1
    )

    return is_last_business_day_se


def encode_labels(se: pd.Series) -> pd.Series:
    label_encoder = LabelEncoder()
    label_transformed = label_encoder.fit_transform(se)

    return pd.Series(label_transformed)


# TODO: Should it be splitted in nodes?
def get_features_and_targets_from_transactions(transactions_df: pd.DataFrame, labels):
    transactions_df["date"] = pd.to_datetime(transactions_df["date"])

    # One hot categorical features
    classification_dummies_df = pd.get_dummies(transactions_df["classification"])

    # Fill gaps for non existent labels
    for label in labels:
        if label not in classification_dummies_df.columns:
            classification_dummies_df[label] = 0

    # Encode binary features
    is_last_business_day_se = calculate_month_last_business_day(transactions_df)
    is_last_business_day_encoded_se = encode_labels(is_last_business_day_se)
    is_income_encoded_se = encode_labels(transactions_df["is_income"])
    # Calculate class frequency
    frequency_se = calculate_class_frequency(transactions_df)

    features = [
        classification_dummies_df,
        is_last_business_day_encoded_se,
        is_income_encoded_se,
        frequency_se,
    ]
    X_transactions_df = pd.concat(features, axis=1)

    return dict(
        transactions=X_transactions_df, transaction_targets=transactions_df["classification"],
    )


def combine_results(
    incoming_transactions_df: pd.DataFrame,
    incoming_labels_df: pd.Series,
    confidence_scores: pd.Series,
) -> pd.DataFrame:

    incoming_transactions_df["classification"] = incoming_labels_df
    incoming_transactions_df["confidence"] = confidence_scores

    return incoming_transactions_df


def connect_single_letters(text: str) -> str:
    """Example: "D E Y A  SITEIAS" is converted to "DEYA  SITEIAS" """
    pattern = re.compile(r"\b([a-z]) (?=[a-z]\b)", re.I)

    return re.sub(pattern, r"\g<1>", text)


def clean_website_name(text: str) -> str:
    """Example: "www.eshop.gr" is converted to "eshop" """
    list_of_words = []
    if text is not np.nan:
        for word in text.split():
            word = word.replace("www.", "")
            word = word.replace("WWW.", "")
            word = word.replace("WWW", "")
            word = word.replace("www", "")
            word = word.replace(".gr", "")
            word = word.replace(".GR", "")
            word = word.replace(".com", "")
            word = word.replace(".COM", "")
            list_of_words.append(word)
        words = " ".join(list_of_words)
    else:
        words = ""

    return words


def remove_punctuation(text: str) -> str:
    """Removes punctations from descriptions"""
    word_list = []
    for words in text:
        characters_list = []
        for characters in words:
            if characters not in string.punctuation:
                characters_list.append(characters)
            else:
                if characters == ".":
                    characters_list.append("")
                else:
                    characters_list.append(" ")
        word = "".join(characters_list)
        word_list.append(word)
    words = "".join(word_list)
    return words


def remove_numbers(text: str) -> str:
    """Example: "FYSIKO AERIO VW507" is converted to "FYSIKO AERIO VW" """
    pattern = r"[0-9]"
    # Match all digits in the string and replace them by empty string
    modified_text = re.sub(pattern, "", text)

    return modified_text


def remove_single_letters(text: str) -> str:
    """Removes the single letters included in descriptions"""
    no_letter = [word for word in text.split() if len(word) > 1]
    words_no_letter = " ".join(no_letter)

    return words_no_letter


def clean_description(description: str) -> str:
    """Does the basic cleaning in a description"""
    cleaned_description = clean_website_name(description)
    cleaned_description = remove_punctuation(cleaned_description)
    cleaned_description = connect_single_letters(cleaned_description)
    cleaned_description = remove_numbers(cleaned_description)
    cleaned_description = remove_single_letters(cleaned_description)

    return cleaned_description


def clean_descriptions(transactions_df: pd.DataFrame, is_training: bool = False) -> pd.DataFrame:
    """
    Combines all the basic cleaning functions. Also used the old one cleaning
    function used for incoming transactions.

    Clean description function includes: clean_website_name, remove_punctuation,
    connect_single_letters, remove_numbers, remove_single_letters

    Clean transaction descriptions includes: remove_accent_and_lowercase,
    convert_characters
    """

    transactions_df["processed_description"] = transactions_df["processed_description"].apply(str)
    # Use the unique ones and then merge it in order to run quickly
    unique_descriptions_df = transactions_df.processed_description.drop_duplicates().reset_index(
        drop=True
    )
    processed_unique_descriptions_df = unique_descriptions_df.copy()
    processed_unique_descriptions_df = pd.DataFrame(processed_unique_descriptions_df)

    # Check if is training or not in order to use or avoid dask
    if is_training:
        # Try to create one time a dask object and pass all the functions one by one
        processed_unique_descriptions_df["new_description"] = (
            dd.from_pandas(
                processed_unique_descriptions_df["processed_description"],
                npartitions=4 * multiprocessing.cpu_count(),
            )
            .map_partitions(lambda dframe: dframe.apply(lambda row: clean_description(row)))
            .compute(scheduler="processes")
        )
    else:
        processed_unique_descriptions_df[
            "new_description"
        ] = processed_unique_descriptions_df.apply(
            lambda row: clean_description(row["processed_description"]), axis=1
        )

    # Call the old function, as used also in incoming transactions
    cleaned_unique_descriptions_se = clean_transaction_descriptions(
        processed_unique_descriptions_df["new_description"]
    )
    # Replace the old column values with the new
    processed_unique_descriptions_df["new_description"] = cleaned_unique_descriptions_se

    cleaned_descriptions_df = pd.merge(
        transactions_df,
        processed_unique_descriptions_df,
        on=["processed_description", "processed_description"],
    )

    cleaned_descriptions_df = cleaned_descriptions_df[
        ["initial_description", "classification", "new_description"]
    ]
    cleaned_descriptions_df.columns = [
        "initial_description",
        "classification",
        "processed_description",
    ]

    return cleaned_descriptions_df


def drop_empty_rows(transactions_df: pd.DataFrame) -> pd.DataFrame:
    """Drops the empty rows of empty descriptions in a dataframe"""
    dropped_empty_transactions_df = transactions_df.loc[
        transactions_df["processed_description"] != ""
    ]
    dropped_empty_transactions_df = dropped_empty_transactions_df.reset_index(drop=True)

    return dropped_empty_transactions_df


def clean_based_on_bow(transactions_df: pd.DataFrame, bow_to_remove: pd.DataFrame) -> pd.DataFrame:
    """It creates a new transactions_df with a new column of removed names & surnames from description"""

    # Create through re function the pattern of the words from dataframe
    replace_words = re.compile(r"\b(" + ("|".join(bow_to_remove["words"])) + r")\b")
    # Update new_description column with the removed names from descriptions
    transactions_df["new_description"] = transactions_df["new_description"].str.replace(
        replace_words, ""
    )

    return transactions_df


def greek_to_english(description: str) -> str:
    """
    This function converts a greek description (every word) to english (greeklish). The possible greek to english combinations
    for each word are more than 1.
    We choose to first one as calculated by the library.
    """
    if description == "":
        return " "
    converter = Converter(max_expansions=1)

    return converter.convert(u"" + description)[0].lower()


def remove_words(transactions_df: pd.DataFrame, words_df: pd.DataFrame) -> pd.DataFrame:
    """
    This function gets a transactions df and a dataframe of words.
    It firstly changes descriptions to upper
    It excludes the words from words df from each sentence as defined
    from names of selected columns
    """
    replace_words = re.compile(r"\b(" + ("|".join(words_df["word"])) + r")\b")
    transactions_df["processed_description"] = transactions_df["description"].apply(
        lambda row: (row.upper())
    )

    transactions_df["processed_description"] = transactions_df["processed_description"].str.replace(
        replace_words, ""
    )
    transactions_df.columns = ["initial_description", "classification", "processed_description"]

    return transactions_df


def identify_card_transactions_on_mcc(transactions_df: pd.DataFrame) -> pd.DataFrame:
    """
    If a transaction has a non na mcc and is has not already a type as
    debit and credit, it gets as type 'debit' which identifies the transaction
    as a card transaction.
    """
    transactions_df["type"] = transactions_df.apply(
        lambda x: "debit"
        if (not pd.isnull(x["mcc"]))
        & (x["type"] != "debit")
        & (x["type"] != "credit")
        & (x["type"] != "card")
        else x["type"],
        axis=1,
    )

    return transactions_df


def identify_bank_on_iban(iban: str, bank_digits_df: pd.DataFrame) -> str:
    """Identifies bank based on iban"""
    bank_digits = iban[4:7]

    if bank_digits in bank_digits_df["code"].values:
        temp_df = bank_digits_df.loc[bank_digits_df["code"] == bank_digits]
        temp_df = temp_df.reset_index(drop=True)

        return temp_df["bank"][0]
    else:
        return "uknown_bank"


def convert_transactions_on_iban_to_card_type(
    transactions_df: pd.DataFrame, bank_digits_df: pd.DataFrame, bank: str,
) -> pd.DataFrame:
    """
    It gets transactions and based on iban if a specific bank account is identified,
    it changes type of transaction to 'debit'
    """
    transactions_df["type"] = transactions_df.apply(
        lambda x: "debit"
        if (identify_bank_on_iban(x["iban"], bank_digits_df) == bank)
        else x["type"],
        axis=1,
    )

    return transactions_df


def create_transactions_temp_type(transactions_df: pd.DataFrame) -> pd.DataFrame:
    """
    It creates a new temp type for the classification process, in order to pass the appropriate banks'
    transactions to the right classification pipeline. It needs to be dropped at the end of the
    classification pipeline. Actually will rename the initial type until the end of pipeline and will
    rename it again back.
    """
    transactions_df = transactions_df.rename(columns={"type": "initial_type"})
    transactions_df["type"] = transactions_df["initial_type"]

    return transactions_df


def convert_multiple_banks_transactions_to_card_type(
    transactions_df: pd.DataFrame, bank_digits_df: pd.DataFrame, banks_to_change: Dict[str, str]
) -> pd.DataFrame:
    """
    Calls the convert transactions on iban to card type function based in banks to change params
    for each one of them. Currently is used for eurobank and revolut bank.
    """
    # Get a list of the banks in params file
    banks_list = []
    for key, value in banks_to_change.items():
        banks_list.append(value)

    for bank in banks_to_change:
        transactions_df = convert_transactions_on_iban_to_card_type(
            transactions_df, bank_digits_df, bank
        )

    return transactions_df


def combine_pan_iban(pan: str, iban: str) -> str:
    """It gets pan and iban and returns the one of them that is not None"""
    if pd.isnull(iban):
        return create_pan_with_asterisk(pan)
    else:
        return iban


def create_combined_iban_pan(accounts_df: pd.DataFrame) -> pd.DataFrame:
    """
    It takes iban and pan and returns a new column of the one that is
    not None
    """

    accounts_df["iban_pan"] = accounts_df.apply(
        lambda x: combine_pan_iban(x["pan"], x["iban"]), axis=1
    )

    return accounts_df


def get_transactions_type_from_accounts(
    transactions_df: pd.DataFrame, accounts_df: pd.DataFrame
) -> pd.DataFrame:
    """
    Gets as input transactions and accounts and merges them in order to give
    the corresponding type of account to each transaction
    """

    # Process the accounts in order to combine in one column iban (accounts) and pan (cards)
    # Keep only the necessary part which is type and combined iban/pan
    processed_accounts = create_combined_iban_pan(accounts_df)
    processed_accounts = processed_accounts[["type", "iban_pan"]]
    processed_accounts = processed_accounts.drop_duplicates().reset_index(drop=True)

    # Merge transactions with proccessed accounts
    merged_transactions = pd.merge(
        transactions_df, processed_accounts, left_on="iban", right_on="iban_pan", how="left"
    )
    # Replace the old type in transactions with the new one from accounts
    merged_transactions["type"] = merged_transactions["type_y"]
    # Remove the extra fields that merge proccess created
    merged_transactions = merged_transactions.drop(["iban_pan", "type_y", "type_x"], axis=1)

    return merged_transactions


def split_to_card_and_remittance_transactions(
    transactions_df: pd.DataFrame,
) -> Dict[pd.DataFrame, pd.DataFrame]:
    """
    It gets as input outgoing transactions and splits them to card 
    expenses and remittance expenses
    
    """
    card_transactions_df = transactions_df.loc[
        (transactions_df["type"] == "debit")
        | (transactions_df["type"] == "credit")
        | (transactions_df["type"] == "card")
    ]
    card_transactions_df = card_transactions_df.reset_index(drop=True)

    remittance_transactions_df = transactions_df.loc[
        (transactions_df["type"] != "debit")
        & (transactions_df["type"] != "credit")
        & (transactions_df["type"] != "card")
    ]
    remittance_transactions_df = remittance_transactions_df.reset_index(drop=True)

    return dict(
        card_transactions=card_transactions_df, remittance_transactions=remittance_transactions_df,
    )


def split_to_mcc_and_non_mcc_card_expenses(
    transactions_df: pd.DataFrame,
) -> Dict[pd.DataFrame, pd.DataFrame]:
    """
    It gets as input card expenses and splits them to mcc card expenses (if mcc code exists)
    and non mcc card expenses (is mcc code does not exist)
    """

    mcc_card_expenses_df = transactions_df.loc[transactions_df["mcc"].notna()]
    mcc_card_expenses_df = mcc_card_expenses_df.reset_index(drop=True)

    non_mcc_card_expenses_df = transactions_df.loc[transactions_df["mcc"].isna()]
    non_mcc_card_expenses_df = non_mcc_card_expenses_df.reset_index(drop=True)

    return dict(
        mcc_card_expenses_transactions=mcc_card_expenses_df,
        non_mcc_card_expenses_transactions=non_mcc_card_expenses_df,
    )


def remove_intro_of_card_description(description: str) -> str:
    """
    It removes "ΑΓΟΡΑ - " based on nbg card expenses and removes more than one spaces
    Is needed for NBG card descriptions, bevause are duplicated with the simple accounts transactions
    and have differences on descriptions.
    """
    processed_description = re.sub("ΑΓΟΡΑ - +", "", description)
    processed_description = re.sub(" +", " ", processed_description)

    return processed_description


def process_nbg(
    card_expenses: pd.DataFrame,
    remittance_expenses: pd.DataFrame,
    temp_remittance_expenses: pd.DataFrame,
):
    """
    It processes card descriptions and remitance descriptions and finds the duplicated.
    It is based on description, and amount. Amount will be always the same. The description
    has usually some differences that cannot be found easy. So fuzz_partial_levenshtein_similarity
    is also used for the string similarity of the processed descriptions.
    """

    for i, card_row in card_expenses.iterrows():
        for j, rem_row in temp_remittance_expenses.iterrows():

            date_dif_delta = rem_row["date"] - card_row["date"]
            date_dif_days = date_dif_delta.days

            if (date_dif_days <= 7) & (date_dif_days >= 0):
                processed_card_description = remove_intro_of_card_description(
                    card_row["description"]
                )
                fuzz_partial_levenshtein_similarity = (
                    fuzz.partial_ratio(processed_card_description, rem_row["description"]) / 100
                )

                if (fuzz_partial_levenshtein_similarity > 0.86) & (
                    rem_row["amount"] == card_row["amount"]
                ):
                    card_expenses.loc[i, "duplicated_with"] = rem_row["provider_id"]
                    remittance_expenses.loc[j, "duplicated_with"] = card_row["provider_id"]
                    temp_remittance_expenses = temp_remittance_expenses.drop([j])
                    break
            elif date_dif_days > 0:
                break

    return card_expenses, remittance_expenses


def process_alpha(
    card_expenses: pd.DataFrame,
    remittance_expenses: pd.DataFrame,
    temp_remittance_expenses: pd.DataFrame,
):
    """
    It processes card descriptions and remitance descriptions and finds the duplicated.
    It is based on description, and amount. Amount will be always the same. The description
    has usually some differences that cannot be found easy. So fuzz_partial_levenshtein_similarity
    is also used for the string similarity of the processed descriptions.
    """

    for i, card_row in card_expenses.iterrows():
        for j, rem_row in temp_remittance_expenses.iterrows():
            date_dif_delta = rem_row["date"] - card_row["date"]
            date_dif_days = date_dif_delta.days

            if (date_dif_days <= 7) & (date_dif_days >= 0):
                processed_remittance_description = convert_alpha_characters(rem_row["description"])
                fuzz_partial_levenshtein_similarity = (
                    fuzz.partial_ratio(processed_remittance_description, card_row["description"])
                    / 100
                )

                if (fuzz_partial_levenshtein_similarity > 0.86) & (
                    rem_row["amount"] == card_row["amount"]
                ):
                    card_expenses.loc[i, "duplicated_with"] = rem_row["provider_id"]
                    remittance_expenses.loc[j, "duplicated_with"] = card_row["provider_id"]
                    temp_remittance_expenses = temp_remittance_expenses.drop([j])
                    break
            elif date_dif_days > 0:
                break

    return card_expenses, remittance_expenses


def identify_duplicated_transactions(
    card_expenses: pd.DataFrame, remittance_expenses: pd.DataFrame, bank: str
) -> Dict[pd.DataFrame, pd.DataFrame]:
    """
    It creates a new duplicated with column. This column refers to the provider id of the exact same
    transaction. It finds the duplicated transactions between card expenses and remittance expenses.
    In all these functions dates are not used because they are different. Currently this issue has
    beed identified for alphabank and nbg because they are the 2 out of 4 main banks ion Greece that
    give different rpoducts for cards and accounts
    """
    card_expenses["date"] = pd.to_datetime(card_expenses["date"])
    remittance_expenses["date"] = pd.to_datetime(remittance_expenses["date"])

    if "duplicated_with" not in card_expenses.columns:
        card_expenses["duplicated_with"] = None
        remittance_expenses["duplicated_with"] = None

    card_expenses_of_bank_df = card_expenses.loc[card_expenses["bank"] == bank].reset_index(
        drop=True
    )
    remittance_expenses_of_bank_df = remittance_expenses.loc[
        remittance_expenses["bank"] == bank
    ].reset_index(drop=True)

    card_expenses_of_other_bank_df = card_expenses.loc[card_expenses["bank"] != bank].reset_index(
        drop=True
    )
    remittance_expenses_of_other_bank_df = remittance_expenses.loc[
        remittance_expenses["bank"] != bank
    ].reset_index(drop=True)

    card_expenses_of_bank_df = card_expenses_of_bank_df.sort_values("date").reset_index(drop=True)
    remittance_expenses_of_bank_df = remittance_expenses_of_bank_df.sort_values("date").reset_index(
        drop=True
    )

    # Temp remittance expenses for the loop
    temp_remittance_expenses_of_bank_df = remittance_expenses_of_bank_df.copy()

    if bank == "nbg":
        dupl_card_expenses_df, dupl_remittance_expenses_df = process_nbg(
            card_expenses_of_bank_df,
            remittance_expenses_of_bank_df,
            temp_remittance_expenses_of_bank_df,
        )

    if bank == "alpha":
        dupl_card_expenses_df, dupl_remittance_expenses_df = process_alpha(
            card_expenses_of_bank_df,
            remittance_expenses_of_bank_df,
            temp_remittance_expenses_of_bank_df,
        )

    if (bank != "nbg") & (bank != "alpha"):
        dupl_card_expenses_df = pd.DataFrame()
        dupl_remittance_expenses_df = pd.DataFrame()

    final_card_expenses = card_expenses_of_other_bank_df.append(
        dupl_card_expenses_df, ignore_index=True
    )
    final_remittance_expenses = remittance_expenses_of_other_bank_df.append(
        dupl_remittance_expenses_df, ignore_index=True
    )

    return dict(
        identified_duplicated_card_expenses=final_card_expenses,
        identified_duplicated_remittance_expenses=final_remittance_expenses,
    )


def create_pan_with_asterisk(full_pan: str) -> str:
    first = full_pan[0:6]
    second = "******"
    third = full_pan[12:]

    return first + second + third


def combine_cards_with_accounts(
    accounts_df: pd.DataFrame, bank_digits: pd.DataFrame
) -> pd.DataFrame:
    """
    It connects cards with banks and accounts. It finds banks through iban,
    converts pan of cards with appropriate asterisks and merges cards with simple accounts
    through the same consent id. It also gives the found bank name.
    """

    card_accounts = accounts_df.loc[accounts_df["iban"].isna()].reset_index(drop=True)
    simple_accounts = accounts_df.loc[accounts_df["iban"].notna()].reset_index(drop=True)

    card_accounts["pan"] = card_accounts.apply(
        lambda x: create_pan_with_asterisk(x["pan"]) if x["pan"] is not None else None, axis=1
    )

    simple_accounts["bank"] = simple_accounts.apply(
        lambda x: identify_bank_on_iban(x["iban"], bank_digits) if x["iban"] is not None else None,
        axis=1,
    )

    card_accounts_with_bank = pd.merge(
        card_accounts, simple_accounts[["consent_id", "bank"]], on="consent_id", how="left"
    )
    combined_accounts = simple_accounts.append(card_accounts_with_bank, ignore_index=True)
    # Combine iban and pan
    combined_accounts = create_combined_iban_pan(combined_accounts)

    return combined_accounts


def identify_banks(accounts_df: pd.DataFrame, outgoing_transactions_df: pd.DataFrame):
    """
    It merges accounts with transactions in order to connect them through iban and
    find in which bank each transaction is connected
    """
    accounts_df = accounts_df.drop_duplicates(subset="iban_pan")
    merged_transactions_df = pd.merge(
        outgoing_transactions_df,
        accounts_df[["iban_pan", "bank"]],
        left_on="iban",
        right_on="iban_pan",
        how="left",
    )
    merged_transactions_df = merged_transactions_df.drop(["iban_pan"], axis=1)

    return merged_transactions_df.reset_index(drop=True)


def reclassify_duplicated_remittance_expenses(
    remittance_expenses_df: pd.DataFrame, card_expenses_df: pd.DataFrame
) -> pd.DataFrame:
    """
    Gets transactions with duplicated_with attribute and keeps the right classification of the duplicates
    which is the one in cards
    """
    # Split remitance expenses to duplicated and non duplicated
    non_duplicated_remittance_expenses_df = remittance_expenses_df.loc[
        remittance_expenses_df["duplicated_with"].isna()
    ].reset_index(drop=True)
    duplicated_remittance_expenses_df = remittance_expenses_df.loc[
        remittance_expenses_df["duplicated_with"].notna()
    ].reset_index(drop=True)
    # Drop the classification column because will be created a new one
    duplicated_remittance_expenses_df = duplicated_remittance_expenses_df.drop(
        ["classification", "confidence"], axis=1
    )
    # Merge duplicated remitance expenses with card expenses based on the kept ids
    merged_duplicated_remittance_expenses_df = pd.merge(
        duplicated_remittance_expenses_df,
        card_expenses_df[["classification", "provider_id", "confidence"]],
        left_on="duplicated_with",
        right_on="provider_id",
        how="left",
    )
    # Drop the unnecessary created columns and rename the id column
    if "provider_id_y" in merged_duplicated_remittance_expenses_df.columns:
        merged_duplicated_remittance_expenses_df = merged_duplicated_remittance_expenses_df.drop(
            ["provider_id_y"], axis=1
        )
    if "provider_id_x" in merged_duplicated_remittance_expenses_df.columns:
        merged_duplicated_remittance_expenses_df = merged_duplicated_remittance_expenses_df.rename(
            columns={"provider_id_x": "provider_id"}
        )
    # Append to the non duplicated remitance expenses the new classified remittance expenses
    final_remittance_expenses_df = non_duplicated_remittance_expenses_df.append(
        merged_duplicated_remittance_expenses_df, ignore_index=True
    )
    final_remittance_expenses_df = final_remittance_expenses_df.drop(["bank"], axis=1)

    return final_remittance_expenses_df


def reclassify_to_other(
    outgoing_transactions_df: pd.DataFrame, other_category_threshold: float
) -> pd.DataFrame:
    """
    It temporarily reclassifies the given classes of the classification based on the value of
    confidence column (if it classification is not already other). If it is below
    the given threshold of other category, the classification of each corresponding
    row is converted to "other". It is needed only for the grouping of essential/lifestyle/other.
    After it needs to get back the initial classification column
    """
    outgoing_transactions_df["initial_classification"] = outgoing_transactions_df["classification"]
    # Create a mask to filter based on threshold
    mask = (outgoing_transactions_df["confidence"] < other_category_threshold) & (
        outgoing_transactions_df["classification"] != "other"
    )
    # Change the classification of the selected values based on mask
    outgoing_transactions_df["classification"][mask] = "other"

    return outgoing_transactions_df


def adjust_recurring_other_transactions(outgoing_transactions_df: pd.DataFrame) -> pd.DataFrame:
    """
    It changes the group value of transactions that are recurring and their group is other
    to essential
    """
    outgoing_transactions_df.loc[
        (outgoing_transactions_df["is_recurring"] == True)
        & (outgoing_transactions_df["group"] == "other"),
        "group",
    ] = "essential"

    return outgoing_transactions_df


def identify_essential_lifestyle_other(
    outgoing_transactions_df: pd.DataFrame,
    essential_lifestyle_wysely_categories_df: pd.DataFrame,
    other_category_threshold: float,
) -> pd.DataFrame:
    """
    New function for identifying essential, lifestyle and other categories.
    It is based on exact match from wysely categories. If a category is not in
    wysely categories file, connect it with other.
    """
    # Change column name for categories in order to match the classification name
    # of outgoing transactions
    essential_lifestyle_wysely_categories_df.columns = ["classification", "group"]

    # Call the reclassify to other function in order to temp convert the classification of low
    # confidence classified transactions to other
    reclassified_outgoing_transactions_df = reclassify_to_other(
        outgoing_transactions_df, other_category_threshold
    )

    # Merge outgoing transactions with categories, in order to get type of expense
    # in outgoing transactions
    if "group" in reclassified_outgoing_transactions_df.columns:
        reclassified_outgoing_transactions_df = reclassified_outgoing_transactions_df.drop(
            ["group"], axis=1
        )

    outgoing_transactions_with_group_df = pd.merge(
        reclassified_outgoing_transactions_df,
        essential_lifestyle_wysely_categories_df,
        on=["classification", "classification"],
        how="left",
    )
    adjusted_recurring_outgoing_transactions_df = adjust_recurring_other_transactions(
        outgoing_transactions_with_group_df
    )
    # Return back the initial classification column as classification and drop the temp classification column
    adjusted_recurring_outgoing_transactions_df = adjusted_recurring_outgoing_transactions_df.drop(
        ["classification"], axis=1
    )
    adjusted_recurring_outgoing_transactions_df = adjusted_recurring_outgoing_transactions_df.rename(
        columns={"initial_classification": "classification"}
    )

    return adjusted_recurring_outgoing_transactions_df


def check_similarity(transactions_df, description, threshold=0.70):
    """
    Check similarity in descriptions of transactions and a given description.
    If similarity is below threshold drop the corresponding row of the returned dataframe
    """
    for index, row in transactions_df.iterrows():
        fuzz_partial_levenshtein_similarity = (
            fuzz.partial_ratio(row["description"], description) / 100
        )
        if fuzz_partial_levenshtein_similarity < threshold:
            transactions_df = transactions_df.loc[
                transactions_df["description"] != row["description"]
            ]
            transactions_df = transactions_df.reset_index(drop=True)
    if len(transactions_df):
        return transactions_df.reset_index(drop=True)

    return pd.DataFrame()


def keep_description_classification(transactions_df):
    """Keeps only description and classification of transactions"""
    transactions_df["classification"] = (
        transactions_df["classification"] if "classification" in transactions_df.columns else None
    )

    return transactions_df[["description", "classification"]]


def calculate_bounded_dates(date, months_window, days_window=3):
    """
    Calculates bounded dates min and max in months and days window
    Params:
    date-> is the given date
    months_window-> how many months to go in front or back
    days_window-> how many days to go in front or back
    """
    bound_date = date + dateutil.relativedelta.relativedelta(months=months_window)
    bound_date_max = bound_date + dateutil.relativedelta.relativedelta(days=days_window)
    bound_date_min = bound_date + dateutil.relativedelta.relativedelta(days=-days_window)

    return bound_date_min, bound_date_max


def combine_card_and_remittance_expenses(card_expenses_df, remittance_epxenses_df):
    """It combines card expenses with remittance expenses in one dataframe"""
    combined_expenses_df = pd.DataFrame()
    combined_expenses_df = combined_expenses_df.append(card_expenses_df, ignore_index=True)
    combined_expenses_df = combined_expenses_df.append(remittance_epxenses_df, ignore_index=True)

    return combined_expenses_df


def combine_transactions_results(
    transactions_df: pd.DataFrame,
    defined_class_transactions: pd.DataFrame,
    labels_df: pd.Series,
    confidence_scores: pd.Series,
    is_training: bool = False,
) -> pd.DataFrame:
    """
    Combines and merges the initial transactions with the new classification
    If is training = True (default) it results to two classification columns in order to campare them(the initial class
    and the new one given(predicted) class)
    If is training = False, it means we need only the classification so we drop the initial class, which will be actually
    None.
    """
    if not is_training:
        # Check if classification or/and confidence is in columns and drop them
        if "classification" in transactions_df.columns:
            transactions_df = transactions_df.drop(["classification"], axis=1)
        if "confidence" in transactions_df.columns:
            transactions_df = transactions_df.drop(["confidence"], axis=1)

    initial_description_df = defined_class_transactions[["initial_description"]]
    # TODO: check if this is the correct solution

    # Connect the predictions with descriptions in one dataframe
    predicted_transactions = pd.concat(
        [initial_description_df, labels_df, confidence_scores], axis=1
    )
    predicted_transactions = predicted_transactions.reset_index(drop=True)
    predicted_transactions.columns = ["description", "classification", "confidence"]
    # Create the unique rows dataframe and merge it with the initial to give the predcitions in classes
    unique_predicted_transactions = predicted_transactions.drop_duplicates(
        subset="description"
    ).reset_index(drop=True)

    final_transactions_df = pd.merge(
        transactions_df, unique_predicted_transactions, on=["description", "description"],
    )

    return final_transactions_df
