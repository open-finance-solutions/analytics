from kedro.pipeline import Pipeline, node

import src.wyre._01_data_engineering.nodes as n01
import src.wyre._02_data_science.nodes as n02
import src.wyre._02_data_science.expenses_nodes as n02_expenses


def create_split_incoming_outgoing_pipeline(**kwargs):
    return Pipeline(
        [
            node(
                func=n01.split_incoming_and_outgoing,
                inputs="input_transactions",
                outputs=dict(
                    incoming_transactions="incoming_transactions",
                    outgoing_transactions="outgoing_transactions",
                ),
            )
        ]
    )


def create_split_outgoing_transactions_pipeline(**kwargs):
    return Pipeline(
        [
            # TODO: Check maybe to temp keep iban_pan combination, beacuse is calculated again in
            # combine_cards_with_accounts as below
            # TODO: Also do we want the same for incoming transactions to get a type
            # Maybe will be needed in an edge case of having a reversal of a card purchase
            node(
                func=n01.get_transactions_type_from_accounts,
                inputs=["outgoing_transactions", "input_accounts"],
                outputs="outgoing_transactions_with_account_type",
            ),
            node(
                func=n01.create_transactions_temp_type,
                inputs="outgoing_transactions_with_account_type",
                outputs="outgoing_transactions_with_temp_type",
            ),
            node(
                func=n01.identify_card_transactions_on_mcc,
                inputs="outgoing_transactions_with_temp_type",
                outputs="identified_outgoing_transactions_on_mcc",
            ),
            # TODO: Check if we want to keep a flag for eurobank transactions, in order to
            # have bag of words for rents, loans etc. because after this step will go into
            # neural network of card expenses and not include rent, loans etc.
            node(
                func=n01.convert_multiple_banks_transactions_to_card_type,
                inputs=[
                    "identified_outgoing_transactions_on_mcc",
                    "bank_digits",
                    "params:banks_to_change",
                ],
                outputs="identified_outgoing_transactions",
            ),
            node(
                func=n01.combine_cards_with_accounts,
                inputs=["input_accounts", "bank_digits"],
                outputs="combined_accounts",
            ),
            node(
                func=n01.identify_banks,
                inputs=["combined_accounts", "identified_outgoing_transactions"],
                outputs="identified_banks_outgoing_transactions",
            ),
            node(
                func=n01.split_to_card_and_remittance_transactions,
                inputs="identified_banks_outgoing_transactions",
                outputs=dict(
                    card_transactions="card_expenses_transactions",
                    remittance_transactions="remittance_expenses_transactions",
                ),
            ),
            # TODO: This could be splited in order to run in parallel
            # TODO: Also the duplicated remittance expenses could not be passed through the remitance model
            # beacuse in the next steps will be reclassified (reclassify_duplicated_remittance_expenses)
            node(
                func=n01.identify_duplicated_transactions,
                inputs=[
                    "card_expenses_transactions",
                    "remittance_expenses_transactions",
                    "params:nbg_bank_name",
                ],
                outputs=dict(
                    identified_duplicated_card_expenses="identified_duplicated_nbg_card_expenses",
                    identified_duplicated_remittance_expenses="identified_duplicated_nbg_remittance_expenses",
                ),
            ),
            node(
                func=n01.identify_duplicated_transactions,
                inputs=[
                    "identified_duplicated_nbg_card_expenses",
                    "identified_duplicated_nbg_remittance_expenses",
                    "params:alpha_bank_name",
                ],
                outputs=dict(
                    identified_duplicated_card_expenses="identified_duplicated_card_expenses",
                    identified_duplicated_remittance_expenses="identified_duplicated_remittance_expenses",
                ),
            ),
            node(
                func=n01.split_to_mcc_and_non_mcc_card_expenses,
                inputs="identified_duplicated_card_expenses",
                outputs=dict(
                    mcc_card_expenses_transactions="mcc_card_expenses_transactions",
                    non_mcc_card_expenses_transactions="non_mcc_card_expenses_transactions",
                ),
            ),
        ]
    )


def create_classify_incoming_descriptions_pipeline(**kwargs):
    return Pipeline(
        [
            node(
                func=n01.keep_description_classification,
                inputs="incoming_transactions",
                outputs="selected_incoming_transactions",
            ),
            node(
                func=n01.remove_words,
                inputs=["selected_incoming_transactions", "card_expenses_words_to_remove"],
                outputs="removed_words_incoming_transactions",
            ),
            node(
                func=n01.clean_descriptions,
                inputs="removed_words_incoming_transactions",
                outputs="cleaned_incoming_descriptions",
            ),
            node(
                func=n02_expenses.convert_descriptions_to_english,
                inputs="cleaned_incoming_descriptions",
                outputs="english_converted_incoming_transactions",
            ),
            node(
                func=n02_expenses.stem_english_descriptions,
                inputs="english_converted_incoming_transactions",
                outputs="stemmed_english_incoming_transactions",
            ),
            node(
                func=n01.get_descriptions_and_targets_from_transactions,
                inputs="stemmed_english_incoming_transactions",
                outputs=dict(
                    descriptions="incoming_descriptions",
                    description_targets="incoming_description_targets",
                ),
            ),
            node(
                func=n01.pad_description_sequences,
                inputs=[
                    "incoming_description_tokenizer",
                    "incoming_descriptions",
                    "params:incoming_description_classifier_model_params",
                ],
                outputs="incoming_descriptions_as_padded_sequences",
            ),
            node(
                func=n02.predict_with_classifier,
                inputs=[
                    "incoming_description_classifier_model",
                    "incoming_descriptions_as_padded_sequences",
                    "incoming_description_classifier_y_labels",
                ],
                outputs=dict(
                    predictions="incoming_description_predictions",
                    prediction_labels="incoming_description_prediction_labels",
                    confidence_scores="incoming_confidence_scores",
                ),
            ),
            node(
                func=n01.combine_transactions_results,
                inputs=[
                    "incoming_transactions",
                    "stemmed_english_incoming_transactions",
                    "incoming_description_prediction_labels",
                    "incoming_confidence_scores",
                ],
                outputs="basic_classified_incoming_transactions",
            ),
        ]
    )


def create_classify_mcc_card_transactions_pipeline(**kwargs):
    return Pipeline(
        [
            node(
                func=n02.classify_transactions_on_mcc,
                inputs=["mcc_card_expenses_transactions", "mcc_codes_to_wysely_categories"],
                outputs="mcc_card_expenses_output_transactions",
            ),
        ]
    )


def create_classify_non_mcc_card_descriptions_pipeline(**kwargs):
    return Pipeline(
        [
            node(
                func=n01.keep_description_classification,
                inputs="non_mcc_card_expenses_transactions",
                outputs="selected_card_expenses_transactions",
            ),
            node(
                func=n01.remove_words,
                inputs=["selected_card_expenses_transactions", "card_expenses_words_to_remove"],
                outputs="removed_words_non_mcc_card_transactions",
            ),
            node(
                func=n01.clean_descriptions,
                inputs="removed_words_non_mcc_card_transactions",
                outputs="cleaned_non_mcc_card_descriptions",
            ),
            node(
                func=n02_expenses.convert_descriptions_to_english,
                inputs="cleaned_non_mcc_card_descriptions",
                outputs="english_converted_transactions",
            ),
            node(
                func=n02_expenses.stem_english_descriptions,
                inputs="english_converted_transactions",
                outputs="stemmed_english_transactions",
            ),
            node(
                func=n01.get_descriptions_and_targets_from_transactions,
                inputs="stemmed_english_transactions",
                outputs=dict(
                    descriptions="descriptions", description_targets="description_targets",
                ),
            ),
            node(
                func=n01.pad_description_sequences,
                inputs=[
                    "card_expenses_description_tokenizer",
                    "descriptions",
                    "params:card_expenses_description_classifier_model_params",
                ],
                outputs="non_mcc_card_descriptions_as_padded_sequences",
            ),
            node(
                func=n02_expenses.predict_with_card_expenses_classifier,
                inputs=[
                    "card_expenses_description_classifier_model",
                    "non_mcc_card_descriptions_as_padded_sequences",
                    "card_expenses_description_classifier_y_labels",
                ],
                outputs=dict(
                    predictions="card_expenses_description_predictions",
                    prediction_labels="card_expenses_description_prediction_labels",
                    confidence_scores="non_mcc_card_descriptions_confidence_scores",
                ),
            ),
            node(
                func=n01.combine_transactions_results,
                inputs=[
                    "non_mcc_card_expenses_transactions",
                    "stemmed_english_transactions",
                    "card_expenses_description_prediction_labels",
                    "non_mcc_card_descriptions_confidence_scores",
                ],
                outputs="non_mcc_card_expenses_output_transactions",
            ),
        ]
    )


def create_classify_remittance_expenses_descriptions_pipeline(**kwargs):
    return Pipeline(
        [
            node(
                func=n01.keep_description_classification,
                inputs="identified_duplicated_remittance_expenses",
                outputs="selected_remittance_expenses_transactions",
            ),
            node(
                func=n01.remove_words,
                inputs=[
                    "selected_remittance_expenses_transactions",
                    "card_expenses_words_to_remove",
                ],
                outputs="removed_words_remittance_expenses_transactions",
            ),
            node(
                func=n01.clean_descriptions,
                inputs="removed_words_remittance_expenses_transactions",
                outputs="cleaned_transactions",
            ),
            node(
                func=n02_expenses.convert_descriptions_to_english,
                inputs="cleaned_transactions",
                outputs="english_converted_remittance_expenses_transactions",
            ),
            node(
                func=n02_expenses.stem_english_descriptions,
                inputs="english_converted_remittance_expenses_transactions",
                outputs="stemmed_english_remittance_expenses_transactions",
            ),
            node(
                func=n01.get_descriptions_and_targets_from_transactions,
                inputs="stemmed_english_remittance_expenses_transactions",
                outputs=dict(
                    descriptions="remittance_expenses_descriptions",
                    description_targets="remittance_expenses_description_targets",
                ),
            ),
            node(
                func=n01.pad_description_sequences,
                inputs=[
                    "remittance_expenses_description_tokenizer",
                    "remittance_expenses_descriptions",
                    "params:remittance_expenses_description_classifier_model_params",
                ],
                outputs="descriptions_as_padded_sequences",
            ),
            node(
                func=n02_expenses.predict_with_card_expenses_classifier,
                inputs=[
                    "remittance_expenses_description_classifier_model",
                    "descriptions_as_padded_sequences",
                    "remittance_expenses_description_classifier_y_labels",
                ],
                outputs=dict(
                    predictions="remittance_expenses_description_predictions",
                    prediction_labels="remittance_expenses_description_prediction_labels",
                    confidence_scores="confidence_scores",
                ),
            ),
            node(
                func=n01.combine_transactions_results,
                inputs=[
                    "identified_duplicated_remittance_expenses",
                    "stemmed_english_remittance_expenses_transactions",
                    "remittance_expenses_description_prediction_labels",
                    "confidence_scores",
                ],
                outputs="remittance_expenses_output_transactions",
            ),
        ]
    )


# TODO: Check if the extra classifications can be splitted more in other pipelines
def create_combine_expenses_results_pipeline(**kwargs):
    return Pipeline(
        [
            node(
                func=n02_expenses.combine_card_expenses,
                inputs=[
                    "mcc_card_expenses_output_transactions",
                    "non_mcc_card_expenses_output_transactions",
                ],
                outputs="card_expenses_output_transactions",
            ),
            node(
                func=n01.reclassify_duplicated_remittance_expenses,
                inputs=[
                    "remittance_expenses_output_transactions",
                    "card_expenses_output_transactions",
                ],
                outputs="reclassified_remittance_expenses_output_transactions",
            ),
            # These 2 outputs are the main outputs of this pipeline
            #  (one is for card expenses and the other for remittance expenses)
            node(
                func=n02_expenses.clean_columns,
                inputs="card_expenses_output_transactions",
                outputs="cleaned_card_expenses_output_transactions",
            ),
            node(
                func=n02_expenses.clean_columns,
                inputs="reclassified_remittance_expenses_output_transactions",
                outputs="cleaned_reclassified_remittance_expenses_output_transactions",
            ),
        ]
    )


# only for outgoing transactions
def create_extra_classification_pipeline(**kwargs):
    return Pipeline(
        [
            # TODO: The recurring node could be an alone pipeline in the top level and run async
            # it does not need the classification results
            node(
                func=n02.classify_reccuring_payments_programmatically,
                inputs="cleaned_reclassified_remittance_expenses_output_transactions",
                outputs="classified_recurring_expenses_transactions",
            ),
            node(
                func=n01.combine_card_and_remittance_expenses,
                inputs=[
                    "cleaned_card_expenses_output_transactions",
                    "classified_recurring_expenses_transactions",
                ],
                outputs="combined_outgoing_expenses_transactions",
            ),
            # This node needs the classification results
            node(
                func=n01.identify_essential_lifestyle_other,
                inputs=[
                    "combined_outgoing_expenses_transactions",
                    "essential_lifestyle_wysely_categories",
                    "params:other_category_threshold",
                ],
                outputs="basic_classified_expenses_transactions",
            ),
        ]
    )


# TODO: Do not need the classification results in order to run.
# So, it can run on the top level async after the split of incoming and outgoing transactions
def create_flag_inner_transfers_pipeline(**kwargs):
    return Pipeline(
        [
            node(
                func=n01.split_to_card_and_remittance_transactions,
                inputs="basic_classified_expenses_transactions",
                outputs=dict(
                    card_transactions="basic_classified_card_expenses_transactions",
                    remittance_transactions="basic_classified_remittance_outgoing_transactions",
                ),
            ),
            node(
                func=n01.flag_inner_transfers,
                inputs=[
                    "basic_classified_incoming_transactions",
                    "basic_classified_remittance_outgoing_transactions",
                ],
                outputs=dict(
                    incoming_transactions="classified_output_incoming_transactions",
                    outgoing_transactions="classified_remittance_outgoing_transactions",
                ),
            ),
            # Maybe should fill in cards with False values in is_inner column?
            node(
                func=n01.combine_card_and_remittance_expenses,
                inputs=[
                    "cleaned_card_expenses_output_transactions",
                    "classified_recurring_expenses_transactions",
                ],
                outputs="outgoing_expenses_transactions",
            ),
        ]
    )


# TODO: If needed
def create_combine_pipeline_results_pipeline(**kwargs):
    return Pipeline(
        [
            node(
                func=n02_expenses.combine_pipeline_results,
                inputs=[
                    "classified_output_incoming_transactions",
                    "outgoing_expenses_transactions",
                ],
                outputs="full_classification_pipeline_output",
            ),
        ]
    )


def create_classify_transactions_full_pipeline():
    split_incoming_outgoing_pipeline = create_split_incoming_outgoing_pipeline()
    split_outgoing_transactions_pipeline = create_split_outgoing_transactions_pipeline()
    classify_incoming_descriptions_pipeline = create_classify_incoming_descriptions_pipeline()
    classify_mcc_card_transactions_pipeline = create_classify_mcc_card_transactions_pipeline()
    classify_non_mcc_card_descriptions_pipeline = (
        create_classify_non_mcc_card_descriptions_pipeline()
    )
    classify_remittance_expenses_descriptions_pipeline = (
        create_classify_remittance_expenses_descriptions_pipeline()
    )
    combine_expenses_results_pipeline = create_combine_expenses_results_pipeline()
    etxra_classification_pipeline = create_extra_classification_pipeline()
    flag_inner_transfers_pipeline = create_flag_inner_transfers_pipeline()
    combine_pipeline_results_pipeline = create_combine_pipeline_results_pipeline()

    classify_transactions_full_pipeline = (
        split_incoming_outgoing_pipeline
        + split_outgoing_transactions_pipeline
        + classify_incoming_descriptions_pipeline
        + classify_mcc_card_transactions_pipeline
        + classify_non_mcc_card_descriptions_pipeline
        + classify_remittance_expenses_descriptions_pipeline
        + combine_expenses_results_pipeline
        + etxra_classification_pipeline
        + flag_inner_transfers_pipeline
        + combine_pipeline_results_pipeline
    )
    return classify_transactions_full_pipeline
