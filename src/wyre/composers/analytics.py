from kedro.pipeline import Pipeline, node

import src.wyre._01_data_engineering.nodes as n01
import src.wyre._03_data_analytics.nodes as n03
from src.wyre._03_data_analytics.report import structure_basic_report

# TODO: This pipeline can be adjusted in order to run
# in parallel with the classification pipelines. Need only to separate in an
# initial stage the types in transactions based on their corresponding accounts
def create_basic_analytics_pipeline(**kwargs):
    return Pipeline(
        [
            node(
                func=n01.split_to_card_and_remittance_transactions,
                inputs="full_classification_pipeline_output",
                outputs=dict(
                    card_transactions="card_transactions",
                    remittance_transactions="remittance_transactions",
                ),
            ),
            node(
                func=n03.remove_out_of_bound_month_transactions,
                inputs="remittance_transactions",
                outputs=dict(
                    bounded_transactions_df="bounded_transactions",
                    upper_bound_date="upper_bound_date",
                    lower_bound_date="lower_bound_date",
                ),
            ),
            node(
                func=n03.calculate_balances_over_time,
                inputs=[
                    "input_accounts",
                    "bounded_transactions",
                    "lower_bound_date",
                    "upper_bound_date",
                ],
                outputs="balances_over_time",
            ),
            node(
                func=n01.split_incoming_and_outgoing,
                inputs="bounded_transactions",
                outputs=dict(
                    incoming_transactions="filtered_incoming_transactions",
                    outgoing_transactions="filtered_outgoing_transactions",
                ),
            ),
            node(
                func=n03.handle_empty_months,
                inputs=[
                    "filtered_incoming_transactions",
                    "lower_bound_date",
                    "upper_bound_date",
                    "params:is_income",
                ],
                outputs="incoming_transactions",
            ),
            node(
                func=n03.handle_empty_months,
                inputs=[
                    "filtered_outgoing_transactions",
                    "lower_bound_date",
                    "upper_bound_date",
                    "params:is_expense",
                ],
                outputs="outgoing_transactions",
            ),
            node(
                func=n03.aggregate_by_month,
                inputs="incoming_transactions",
                outputs="incoming_transactions_by_month",
            ),
            node(
                func=n03.aggregate_by_month,
                inputs="outgoing_transactions",
                outputs="outgoing_transactions_by_month",
            ),
            node(
                func=n03.calculate_profit_loss,
                inputs=["incoming_transactions_by_month", "outgoing_transactions_by_month"],
                outputs="profit_loss",
            ),
            node(
                func=structure_basic_report,
                inputs=[
                    "balances_over_time",
                    "incoming_transactions_by_month",
                    "outgoing_transactions_by_month",
                    "profit_loss",
                ],
                outputs="basic_report",
            ),
        ]
    )
