"""
Include active pipelines here.
Active pipelines are the ones available for cli consuption and package level consumption.
Import pipelines from consposer or lower level pipelines from data processing folders.
"""

from kedro.pipeline import Pipeline
import src.wyre.composers.classification as cl
import src.wyre.composers.training as tr


active_pipelines = {
    # Split
    "split_transactions": cl.create_split_incoming_outgoing_pipeline(),
    "split_outgoing_transactions": cl.create_split_outgoing_transactions_pipeline(),
    # Basic classification
    "classify_incoming_descriptions": cl.create_classify_incoming_descriptions_pipeline(),
    "classify_non_mcc_card_descriptions": cl.create_classify_non_mcc_card_descriptions_pipeline(),
    "classify_mcc_card_transactions": cl.create_classify_mcc_card_transactions_pipeline(),
    "classify_remittance_expenses_descriptions": cl.create_classify_remittance_expenses_descriptions_pipeline(),
    "combine_expenses_results": cl.create_combine_expenses_results_pipeline(),
    # Extra classification
    "extra_classification": cl.create_extra_classification_pipeline(),
    "flag_inner_transfers": cl.create_flag_inner_transfers_pipeline(),
    # Combined parts of classification
    "classify_transactions_full_pipeline": cl.create_classify_transactions_full_pipeline(),
    # Training
    "train_incoming_description_classifier": tr.create_train_incoming_description_classifier_pipeline(),
    "train_card_description_classifier": tr.create_train_card_description_classifier_pipeline(),
    "train_remittance_description_classifier": tr.create_train_remittance_description_classifier_pipeline(),
    "__default__": Pipeline([]),
}
