import pandas as pd
import numpy as np
from datetime import date
from dateutil.relativedelta import relativedelta
from pandas import Series
from typing import Any, Dict, Optional
import dateutil
from calendar import monthrange


def remove_out_of_bound_month_transactions(
    transactions_df: pd.DataFrame, use_upper_bound: bool = False
):
    """
    Gets transactions and stricts them with bounds based on dates
    It cuts the transactions that happened on the first month (-12 months of now)
    If use_upperbound is True it also cuts the last (current) month
    Because this function has a dynamic variable which is max date (is based on current date), in 
    order to test it, can use test_date to replace it
    """
    transactions_df["date"] = pd.to_datetime(transactions_df["date"])
    if "index" in transactions_df.columns:
        del transactions_df["index"]

    # TODO: Instead of using today date, is it better to use the connected accounts and check the date
    # from there? Maybe the date created?
    # Find max date. This date will be the current date each time wyre runs
    max_date = pd.to_datetime(date.today())

    # Find first date of last month and create upper bound (will be closed upper bound)
    if use_upper_bound:
        upper_bound_date = max_date.replace(day=1) + relativedelta(days=-1)
        # Drop all the transactions of last month based on upper bound
        bounded_transactions_df = transactions_df.loc[transactions_df["date"] <= upper_bound_date]
        bounded_transactions_df = bounded_transactions_df.reset_index(drop=True)
    else:
        upper_bound_date = max_date

    # Find min date. This date will be the current date minus 12 months back
    min_date = max_date + relativedelta(months=-12)
    # Find first day of next month of min_date and create lower bound (will be closed lower bound)
    lower_bound_date = (min_date + relativedelta(months=1)).replace(day=1)
    # Drop transactions before the lower bound
    bounded_transactions_df = transactions_df.loc[
        transactions_df["date"] >= lower_bound_date
    ].reset_index(drop=True)

    return dict(
        bounded_transactions_df=bounded_transactions_df,
        upper_bound_date=upper_bound_date,
        lower_bound_date=lower_bound_date,
    )


def calculate_balances_over_time(
    accounts_df: pd.DataFrame,
    transactions_df: pd.DataFrame,
    lower_bound_date,
    upper_bound_date,
    by_month: bool = True,
) -> list:

    """
    Calculates balances over time through the months. 
    If by_month=True the aggregation is by month, else is by day
    """
    # Need to fill the zero transaction months. So use of handle zero transactions months function
    filled_transactions_df = handle_empty_months(
        transactions_df, lower_bound_date, upper_bound_date, True
    )
    # Add sign to amounts
    amounts_with_signs_se = filled_transactions_df.apply(
        lambda t: t.amount if t.is_income else -t.amount, axis=1
    )
    filled_transactions_df.amount = amounts_with_signs_se

    # Transform date to datetime
    booking_date_se = filled_transactions_df.date
    filled_transactions_df.date = pd.to_datetime(booking_date_se)

    # Aggregate by month or by day
    if by_month:
        grouper = pd.Grouper(key="date", freq="M")
    else:
        grouper = pd.Grouper(key="date", freq="D")

    grouped_df = filled_transactions_df.groupby(grouper)
    aggregated_df = grouped_df.amount.sum().reset_index()

    # Set balances as float
    accounts_df["balance"] = accounts_df["balance"].astype(float)
    # Some banks contain cards as accounts, so we drop these from balances calculation
    non_card_accounts_df = accounts_df.loc[
        (accounts_df["type"] != "debit")
        & (accounts_df["type"] != "credit")
        & (accounts_df["type"] != "card")
    ]

    # Subtract every month from balance
    balance = non_card_accounts_df["balance"].sum()
    balances_over_time = []

    # Find beginning balance
    beginning_balance = balance - aggregated_df[1:]["amount"].sum()
    previous_balance = beginning_balance

    for index, row in aggregated_df.iterrows():
        date, amount = row
        if index == 0:
            balance = round(beginning_balance, 2)

        else:
            balance = round(previous_balance + amount, 2)
            previous_balance = balance
        balances_over_time.append(dict(amount=balance, period=date.date().strftime("%m-%Y")))

    return balances_over_time


def handle_empty_months(
    transactions_df: pd.DataFrame, lower_date, upper_date, is_income
) -> pd.DataFrame:
    """
    This function takes as imput transactions and min-max dates and checks 
    if there are months with zero transactions. 
    If yes, it adds a zero amount "synthetic" transaction to the found months

    """

    # Adjust lower and upper date because date_range takes open lower bound
    new_lower_date = pd.to_datetime(lower_date)
    new_lower_date = new_lower_date.replace(day=1)
    new_upper_date = pd.to_datetime(upper_date)
    new_upper_date = new_upper_date.replace(day=1)

    transactions_df = transactions_df.reset_index(drop=True)
    transactions_df["date"] = pd.to_datetime(transactions_df["date"])

    # Find which dates (eg. 2020-01-01) had zero transactions
    real_dates_df = transactions_df.set_index("date").resample("MS").size()
    real_dates_df = real_dates_df.loc[real_dates_df != 0]
    empty_month_dates = pd.date_range(
        start=new_lower_date, end=new_upper_date, freq="MS"
    ).difference(real_dates_df.index)
    empty_month_dates_df = pd.DataFrame(empty_month_dates)
    empty_month_dates_df.columns = ["date"]

    # For each date that found with no transactions, create an empty transaction and append it to transactions_df
    for i, row in empty_month_dates_df.iterrows():
        if transactions_df.empty:
            transactions_df = transactions_df.append(
                {
                    "amount": 0,
                    "is_income": is_income,
                    "date": row["date"],
                    "classification": "synthetic",
                    "description": "SYNTHETIC_TRANSACTION",
                    "confidence": 0.99,
                    "is_inner": False,
                },
                ignore_index=True,
            )
        else:
            new_transaction_df = transactions_df[0:1].copy()
            new_transaction_df["amount"] = 0
            new_transaction_df["date"] = row["date"]
            # TODO: Check classifications id another one does not exist will be false presented
            new_transaction_df["classification"] = "synthetic"
            new_transaction_df["description"] = "SYNTHETIC_TRANSACTION"
            transactions_df = transactions_df.append(new_transaction_df).reset_index(drop=True)

    return transactions_df


def aggregate_by_month(transactions_df: pd.DataFrame) -> pd.DataFrame:
    if transactions_df.empty:
        return pd.DataFrame()
    transactions_df = transactions_df.reset_index(drop=True)
    # Transform date to datetime
    transactions_df["date"] = pd.to_datetime(transactions_df["date"])

    # Group by month
    month_grouper = pd.Grouper(key="date", freq="M")
    grouped_df = transactions_df.groupby(month_grouper)

    # Aggregate
    aggregated_df = grouped_df["amount"].agg(["count", "sum"]).reset_index()

    # Rename column names and round amounts
    aggregated_df.columns = ["date", "number_of_transactions", "amount"]
    aggregated_df["amount"] = aggregated_df["amount"].round(2)
    aggregated_df["date"] = aggregated_df["date"].dt.strftime("%m-%Y")

    return aggregated_df


def calculate_profit_loss(inflows_per_freq, outflows_per_freq):
    """Inflows per freq minus outflows per freq"""

    profit_loss_df = pd.DataFrame()
    profit_loss_df["amount"] = inflows_per_freq["amount"] - outflows_per_freq["amount"]
    profit_loss_df["date"] = inflows_per_freq["date"]

    return profit_loss_df
