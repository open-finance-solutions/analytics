import json
from typing import Any, List
import pandas as pd


def structure_basic_report(
    balances_over_time,
    incoming_transactions_by_month,
    outgoing_transactions_by_month,
    profit_loss_by_freq,
):

    balances = dict(over_time=balances_over_time)
    inflows = format_per_month(incoming_transactions_by_month)
    outflows = format_per_month(outgoing_transactions_by_month)
    profit_loss = format_per_month(profit_loss_by_freq)
    basic_report = dict(
        balances=balances, inflows=inflows, outflows=outflows, profit_loss=profit_loss
    )

    return basic_report


def format_per_month(transactions_by_month: pd.DataFrame) -> List[Any]:
    per_month = [
        dict(period=t["date"], amount=t["amount"]) for i, t in transactions_by_month.iterrows()
    ]
    return per_month
