import pandas as pd
from pandas.testing import assert_series_equal
from kedro.runner import SequentialRunner
from kedro.io import DataCatalog, MemoryDataSet
from src.serve.catalogs import get_data_catalog, get_income_training_catalog
import src.wyre.composers.classification as cl
import src.wyre.composers.training as tr
import json
from sklearn.metrics import accuracy_score


customer1_transactions_df = pd.read_csv("data/01_raw/test/customer_1.csv")
runner = SequentialRunner()

input_accounts = pd.read_csv(
    "data/01_raw/synthetic/input_accounts_for_splitting.csv", dtype={"pan": str}
)
input_transactions = pd.read_csv(
    "data/01_raw/synthetic/input_transactions_for_splitting.csv", dtype={"mcc": str}
)

# TODO: Maybe we have to move somewhere else the training pipeline's tests
class TestPipelines:
    def test_train_incoming_description_classifier(self):
        train_transactions_df = pd.read_csv(
            "data/01_raw/synthetic/training_income_transactions.csv"
        )
        catalog = get_income_training_catalog(train_transactions_df)
        classifier = tr.create_train_incoming_description_classifier_pipeline()
        result = runner.run(classifier, catalog)
        assert result["description_classifier_model_accuracy"] != None

    def test_classify_income(self):
        classifier = cl.create_classify_incoming_descriptions_pipeline()
        incoming_transactions_df = pd.read_csv(
            "data/01_raw/synthetic/training_income_transactions.csv"
        )
        # In order to run the test shuffle the dataframe and check accuracy
        test_incoming_transactions_df = incoming_transactions_df.sample(frac=1)
        test_incoming_transactions_df = test_incoming_transactions_df[0:50].reset_index(drop=True)
        data_catalog = get_data_catalog(
            input_transactions=input_transactions, input_accounts=input_accounts
        )
        data_catalog.add("incoming_transactions", MemoryDataSet(test_incoming_transactions_df))
        result = runner.run(classifier, data_catalog)
        incoming_output_transactions_df = result["basic_classified_incoming_transactions"]
        # keep unique to merge and check accuracy
        unique_incoming_output_transactions_df = incoming_output_transactions_df.drop_duplicates(
            subset="description"
        ).reset_index(drop=True)
        final_incoming_transactions_df = pd.merge(
            test_incoming_transactions_df,
            unique_incoming_output_transactions_df,
            on=["description", "description"],
        )
        acc_score = accuracy_score(
            final_incoming_transactions_df["classification_x"].values,
            final_incoming_transactions_df["classification_y"].values,
        )
        assert (acc_score >= 0.90) & (
            len(incoming_output_transactions_df) == len(test_incoming_transactions_df)
        )

    def test_classify_non_mcc_card_expenses(self):
        classifier = cl.create_classify_non_mcc_card_descriptions_pipeline()
        non_mcc_card_expenses_df = pd.read_csv(
            "data/01_raw/bank/training_non_mcc_card_expenses_transactions.csv"
        )

        # In order to run the test shuffle the dataframe and check accuracy
        test_non_mcc_card_expenses_df = non_mcc_card_expenses_df.sample(frac=1)
        test_non_mcc_card_expenses_df = test_non_mcc_card_expenses_df[0:5000].reset_index(drop=True)

        data_catalog = get_data_catalog(
            input_transactions=input_transactions, input_accounts=input_accounts
        )
        # Add non_mcc_card_expenses_transactions in data catalog because is being created
        # from split function and does not exist
        data_catalog.add(
            "non_mcc_card_expenses_transactions", MemoryDataSet(test_non_mcc_card_expenses_df)
        )
        result = runner.run(classifier, data_catalog)
        non_mcc_card_expenses_output_transactions_df = result[
            "non_mcc_card_expenses_output_transactions"
        ]
        # keep unique to merge and check accuracy
        unique_non_mcc_card_expenses_output_transactions_df = non_mcc_card_expenses_output_transactions_df.drop_duplicates(
            subset="description"
        ).reset_index(
            drop=True
        )
        final_non_mcc_card_expenses_df = pd.merge(
            test_non_mcc_card_expenses_df,
            unique_non_mcc_card_expenses_output_transactions_df,
            on=["description", "description"],
        )
        acc_score = accuracy_score(
            final_non_mcc_card_expenses_df["classification_x"].values,
            final_non_mcc_card_expenses_df["classification_y"].values,
        )
        assert (acc_score >= 0.72) & (
            len(non_mcc_card_expenses_output_transactions_df) == len(test_non_mcc_card_expenses_df)
        )

    def test_classify_mcc_card_expenses(self):
        classifier = cl.create_classify_mcc_card_transactions_pipeline()
        mcc_card_expenses_df = pd.read_csv(
            "data/01_raw/synthetic/mcc_card_expenses_transactions.csv", dtype={"mcc": str}
        )
        data_catalog = get_data_catalog(
            input_transactions=input_transactions, input_accounts=input_accounts
        )
        data_catalog.add("mcc_card_expenses_transactions", MemoryDataSet(mcc_card_expenses_df))
        result = runner.run(classifier, data_catalog)
        mcc_card_expenses_output_transactions_df = result["mcc_card_expenses_output_transactions"]
        # Create the right classification column of known test data
        data = [
            ["household items/supplies"],
            ["household items/supplies"],
            ["insurance and pension"],
            ["insurance and pension"],
            ["eating out and take aways"],
            ["insurance and pension"],
            ["telecommunications"],
            ["insurance and pension"],
            ["insurance and pension"],
            ["fun and leisure"],
        ]
        known_classified_df = pd.DataFrame()
        known_classified_df = known_classified_df.append(data)
        known_classified_df.columns = ["classification"]
        known_classified_series = known_classified_df["classification"]

        assert_series_equal(
            mcc_card_expenses_output_transactions_df["classification"], known_classified_series
        )

    def test_classify_remittance_expenses(self):
        classifier = cl.create_classify_remittance_expenses_descriptions_pipeline()
        remittance_expenses_df = pd.read_csv(
            "data/01_raw/bank/training_remittance_expenses_transactions.csv"
        )
        # In order to run the test shuffle the dataframe and check accuracy
        test_remittance_expenses_df = remittance_expenses_df.sample(frac=1)
        test_remittance_expenses_df = test_remittance_expenses_df[0:50].reset_index(drop=True)
        data_catalog = get_data_catalog(
            input_transactions=input_transactions, input_accounts=input_accounts
        )
        data_catalog.add(
            "identified_duplicated_remittance_expenses", MemoryDataSet(test_remittance_expenses_df)
        )
        result = runner.run(classifier, data_catalog)
        remittance_expenses_output_transactions_df = result[
            "remittance_expenses_output_transactions"
        ]
        # keep unique to merge and check accuracy
        unique_remittance_expenses_output_transactions_df = remittance_expenses_output_transactions_df.drop_duplicates(
            subset="description"
        ).reset_index(
            drop=True
        )
        final_remittance_expenses_df = pd.merge(
            test_remittance_expenses_df,
            unique_remittance_expenses_output_transactions_df,
            on=["description", "description"],
        )
        acc_score = accuracy_score(
            final_remittance_expenses_df["classification_x"].values,
            final_remittance_expenses_df["classification_y"].values,
        )
        assert (acc_score >= 0.90) & (
            len(remittance_expenses_output_transactions_df) == len(test_remittance_expenses_df)
        )

    def test_split_outgoing_transactions_pipeline(self):

        data_catalog = get_data_catalog(
            input_transactions=input_transactions, input_accounts=input_accounts
        )
        # Use the incoming/outgoing splitting pipeline
        splitter_pipeline = cl.create_split_incoming_outgoing_pipeline()
        first_result = runner.run(splitter_pipeline, data_catalog)
        outgoing_transactions_df = first_result["outgoing_transactions"]
        # Add the above result to data catalog
        data_catalog.add("outgoing_transactions", MemoryDataSet(outgoing_transactions_df))

        # Use the main pipeline
        split_outgoing_transactions_pipeline = cl.create_split_outgoing_transactions_pipeline()

        final_result = runner.run(split_outgoing_transactions_pipeline, data_catalog)

        mcc_card_expenses_transactions = final_result["mcc_card_expenses_transactions"]
        non_mcc_card_expenses_transactions = final_result["non_mcc_card_expenses_transactions"]
        remittance_expenses = final_result["identified_duplicated_remittance_expenses"]

        # Assert based on sum of amount column. Could use len for each category but
        # sum will be more strict in case something changes but len remains the same
        # Eurobank expenses are currently calculated as card expenses
        assert round(mcc_card_expenses_transactions["amount"].sum(), 2) == 193.33
        assert round(non_mcc_card_expenses_transactions["amount"].sum(), 2) == 63.9
        assert round(remittance_expenses["amount"].sum(), 2) == 127.6

    def test_combine_expenses_results_pipeline(self):
        mcc_card_expenses_df = pd.read_csv(
            "data/02_intermediate/outgoing_transactions_output/mcc_card_expenses_output_transactions.csv",
            dtype={"mcc": str},
        )
        non_mcc_card_expenses_df = pd.read_csv(
            "data/02_intermediate/outgoing_transactions_output/non_mcc_card_expenses_output_transactions.csv"
        )
        remittance_expenses_df = pd.read_csv(
            "data/02_intermediate/outgoing_transactions_output/remittance_expenses_output_transactions.csv"
        )
        data_catalog = get_data_catalog(
            input_transactions=input_transactions, input_accounts=input_accounts
        )
        data_catalog.add(
            "mcc_card_expenses_output_transactions", MemoryDataSet(mcc_card_expenses_df)
        )
        data_catalog.add(
            "non_mcc_card_expenses_output_transactions", MemoryDataSet(non_mcc_card_expenses_df)
        )
        data_catalog.add(
            "remittance_expenses_output_transactions", MemoryDataSet(remittance_expenses_df)
        )

        combine_expenses_pipeline = cl.create_combine_expenses_results_pipeline()
        result = runner.run(combine_expenses_pipeline, data_catalog)
        remittance_expenses_output_transactions = result[
            "cleaned_reclassified_remittance_expenses_output_transactions"
        ]
        card_expenses_output_transactions = result["cleaned_card_expenses_output_transactions"]

        other_category_remittance = remittance_expenses_output_transactions.loc[
            remittance_expenses_output_transactions["classification"] == "other"
        ]
        fees_category_remitance = remittance_expenses_output_transactions.loc[
            remittance_expenses_output_transactions["classification"] == "fees/charges/taxes"
        ]
        eating_remitance = remittance_expenses_output_transactions.loc[
            remittance_expenses_output_transactions["classification"] == "eating out and take aways"
        ]

        assert round(remittance_expenses_output_transactions["amount"].sum(), 2) == 127.6
        assert round(card_expenses_output_transactions["amount"].sum(), 2) == 257.23
        assert len(other_category_remittance) == 2
        assert len(fees_category_remitance) == 1
        assert len(eating_remitance) == 1

