import pandas as pd
from src.wyre._01_data_engineering.nodes import (
    flag_inner_transfers,
    split_incoming_and_outgoing,
    identify_essential_lifestyle_other,
)

from src.wyre._02_data_science.nodes import (
    classify_reccuring_payments_programmatically,
    classify_transactions_on_mcc,
)
from src.wyre._03_data_analytics.nodes import calculate_balances_over_time


class TestNodes:
    def test_flag_inner_transfers(self):
        customers_df = pd.read_csv("data/01_raw/synthetic/synthetic_transactions.csv")
        # Keep only customer 1
        customer_1 = customers_df.loc[customers_df["customer_id"] == 1.0].reset_index(drop=True)

        # Because customer_1 has no inner transactions we change the last 2 of them in order to test
        customer_1["amount"].iloc[-1:] = 600.77
        customer_1["date"].iloc[-1:] = "2020-04-30"
        customer_1["amount"].iloc[-2:-1] = 599.02
        customer_1["date"].iloc[-2:-1] = "2020-12-18"
        # Need to split incoming and outgoing
        customer_1_transactions = split_incoming_and_outgoing(customer_1)
        incoming_transactions = customer_1_transactions["incoming_transactions"]
        outgoing_transactions = customer_1_transactions["outgoing_transactions"]

        result_transactions_dict = flag_inner_transfers(
            incoming_transactions, outgoing_transactions
        )
        result_incoming_df = result_transactions_dict["incoming_transactions"]
        result_outgoing_df = result_transactions_dict["outgoing_transactions"]

        result_incoming_inner = result_incoming_df.loc[result_incoming_df["is_inner"] == True]
        result_outgoing_inner = result_outgoing_df.loc[result_outgoing_df["is_inner"] == True]

        assert len(result_incoming_inner) == len(result_outgoing_inner) == 2

    def test_classify_reccuring_payments_programmatically(self):
        # Create a list of predictions to test
        payments = [
            ["IATRIKOS", "2021-03-10", 500, "medical/healthcare", "4343433"],
            ["Netflix a1234", "2021-03-29", 500, "medical/healthcare", "44222"],
            ["Netflix a7474", "2021-04-30", 510, "medical/healthcare", "4555"],
            ["Netflix a8834", "2021-05-30", 490, "medical/healthcare", "556"],
            ["HEALTH DEP", "2021-06-30", 400, "medical/healthcare", "443"],
            ["IATROS", "2021-07-10", 490, "medical/healthcare", "888"],
            ["NIKOS PAPADOPOULOS", "2021-09-05", 490, "medical/healthcare", "6667777"],
        ]
        # Create the next month income dataframe
        payments_df = pd.DataFrame(
            payments, columns=["description", "date", "amount", "classification", "provider_id"]
        )
        payments_df["date"] = pd.to_datetime(payments_df["date"])

        result_df = classify_reccuring_payments_programmatically(payments_df)
        false_count = len(result_df) - result_df["is_recurring"].sum()

        assert false_count == 4

    def test_identify_essential_lifestyle_other(self):
        outgoing_transactions_df = pd.read_csv("data/01_raw/synthetic/expenses_for_group_test.csv")
        category_df = pd.read_csv(
            "data/04_feature/bag_of_words/essential_lifestyle_wysely_categories.csv"
        )
        other_category_threshold = 0.50
        identified_outgoing_transactions_df = identify_essential_lifestyle_other(
            outgoing_transactions_df, category_df, other_category_threshold
        )
        essential_count = len(
            identified_outgoing_transactions_df.loc[
                identified_outgoing_transactions_df["group"] == "essential"
            ]
        )
        lifestyle_count = len(
            identified_outgoing_transactions_df.loc[
                identified_outgoing_transactions_df["group"] == "lifestyle"
            ]
        )
        other_count = len(
            identified_outgoing_transactions_df.loc[
                identified_outgoing_transactions_df["group"] == "other"
            ]
        )
        assert essential_count == 40
        assert lifestyle_count == 5
        assert other_count == 5

    def test_calculate_balances_over_time(self):
        transaction_data = [
            ["GR2801725050005703737347565", "2021-07-11", 100, True, "Send money", "checking"],
            # ["25252678888929", "2021-07-11", 100, True, "Send money with card", "debit"],
            ["GR2801725050005703737347565", "2021-08-19", 100, True, "Got money",],
            ["GR2801725050005703737347565", "2021-09-20", 100, False, "Send money", "checking"],
            ["GR2801725050005703737347565", "2021-10-24", 200, False, "Send money", "checking"],
            ["GR2801725050005703737347565", "2021-11-17", 200, True, "Got money",],
        ]

        transactions_df = pd.DataFrame(transaction_data)
        transactions_df.columns = ["iban", "date", "amount", "is_income", "description", "type"]
        # Beacuse there is a dynamic part (current date) need to adjust the given data to the test

        account_data = [
            ["checking", "Payroll", 1200, "GR2801725050005703737347565"],
            ["debit", "Card", 60, "25252678888929"],
        ]
        accounts_df = pd.DataFrame(account_data)
        accounts_df.columns = ["type", "name", "balance", "iban"]
        lower_bound_date = pd.to_datetime("12-01-2020")
        upper_bound_date = pd.to_datetime("18-11-2021")

        balances_over_time_list = calculate_balances_over_time(
            accounts_df, transactions_df, lower_bound_date, upper_bound_date
        )

        list_to_result = [
            {"amount": 1100.0, "period": "12-2020"},
            {"amount": 1100.0, "period": "01-2021"},
            {"amount": 1100.0, "period": "02-2021"},
            {"amount": 1100.0, "period": "03-2021"},
            {"amount": 1100.0, "period": "04-2021"},
            {"amount": 1100.0, "period": "05-2021"},
            {"amount": 1100.0, "period": "06-2021"},
            {"amount": 1200.0, "period": "07-2021"},
            {"amount": 1300.0, "period": "08-2021"},
            {"amount": 1200.0, "period": "09-2021"},
            {"amount": 1000.0, "period": "10-2021"},
            {"amount": 1200.0, "period": "11-2021"},
        ]

        assert balances_over_time_list == list_to_result
