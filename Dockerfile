FROM python:3.7

# install project requirements
COPY ./src/requirements.txt /src/requirements.txt
RUN pip install -r /src/requirements.txt
RUN python -m nltk.downloader punkt popular

COPY . .

EXPOSE 9000

ENTRYPOINT kedro serve
